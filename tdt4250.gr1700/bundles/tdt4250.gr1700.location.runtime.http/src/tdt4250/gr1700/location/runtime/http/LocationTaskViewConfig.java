package tdt4250.gr1700.location.runtime.http;

import org.eclipse.emf.ecore.EClass;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

import no.hal.pg.http.AppConfig;
import tdt4250.gr1700.location.LocationPackage;

@Component(
		service={LocationTaskViewConfig.class, AppConfig.class},
		property={
			HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PREFIX + ":String=/web/location-task",
			HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PATTERN + ":String=/location-task/*"
		}
	)
public class LocationTaskViewConfig implements AppConfig {

	@Override
	public String getMainLocation() {
		return "/location-task/LocationTaskView.html";
	}

	@Override
	public EClass getEClass() {
		return LocationPackage.eINSTANCE.getLocationTaskView();
	}
}
