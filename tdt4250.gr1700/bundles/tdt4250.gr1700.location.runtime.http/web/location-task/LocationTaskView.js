// React component for App

/*
this.props:
{
	locationLatitude: 63.41929, locationLongitude: 10.401694,
	hint: "You are far away...",
	mapView: {
		latitude: 63.41929, longitude: 10.401694, zoom: 13
	}
}

this.state:
{
	locationLatitude: 63.41929, locationLongitude: 10.401694,
	hint: "You are far away...",
	mapView: {
		centerLatitude: 63.41929, centerLongitude: 10.401694, zoom: 13,
		markers: [{
			latitude: 63.41929, longitude: 10.401694,
			text: "You're here",
			radius: 0, // > 0 = circle, < 0 = circle marker, 0 or undefined = marker
		}],
	}
}
 */
class LocationTaskView extends React.Component {

	constructor(props) {
		super(props);
		this.state = this.props;
		// make sure this in callback functions refers to this
		this.setPlayerPosition = this.setPlayerPosition.bind(this);
		this.navigate = this.navigate.bind(this);
		this.tryLocation = this.tryLocation.bind(this);
		this.navigateReturnUrl = this.navigateReturnUrl.bind(this);
	}

	displayName() {
		return "Location task view";
	}

	setPlayerPosition(position) {
		AppUtils.log("Player location changed: " + position);
		var latitude = position.coords.latitude, longitude = position.coords.longitude;
		var newState = {
			locationLatitude: latitude, locationLongitude: longitude, 
		};
		this.setState(newState);
		var comp = this;
		var updateUrl = this.props.dataUrl + "/setPlayerLocation?latitude=" + latitude + "&longitude=" + longitude;
		AppUtils.loadData(updateUrl, false, newState, function(locationTaskView) {
			comp.setState(locationTaskView);
		});
	}
	
	navigate(center, zoom) {
		AppUtils.submitData(this.props.dataUrl + "/mapView/navigate?latitude=" + center.lat + "&longitude=" + center.lng + "&zoom=" + zoom);
	}

	tryLocation() {
		AppUtils.log("Trying player location: " + this.state.locationLatitude + ", " + this.state.locationLongitude);
		var comp = this;
		var updateUrl = this.props.dataUrl + "/tryFinish";
		AppUtils.loadData(updateUrl, false, false, function(finished) {
			comp.setState({ isFinished: finished });
		});
	}

	navigateReturnUrl() {
		if (typeof this.props.returnUrl !== 'undefined') {
			window.location.href = this.props.returnUrl;
		}
	}	

	render() {
		var mapViewProps = {
			centerLatitude: this.state.mapView.latitude, centerLongitude: this.state.mapView.longitude,
			zoom: this.state.mapView.zoom,
			markers: [{
				latitude: this.state.locationLatitude, longitude: this.state.locationLongitude,
				text: "You're here",
				radius: undefined,
				color: "yellow"
			}],
			clientPositionHandler: this.setPlayerPosition,
			// markerPositionHandler: ...
			navigateHandler: this.navigate
		};
		var buttonText = (this.state.isFinished ? "Improve your location" : "Try your location");
		return React.createElement("div", { className: "locationTask" },
			React.createElement("div", { className: "map" },
				React.createElement(MapView, mapViewProps)
			),
			React.createElement("h3", { className: "hint" }, this.state.hint),
			React.createElement("button", { onClick: this.tryLocation }, buttonText),
			React.createElement("button", { className: "back", onClick: this.navigateReturnUrl }, "Back to task list"),
		);
	}
}
