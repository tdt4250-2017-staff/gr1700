/**
 */
package tdt4250.gr1700.location.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import no.hal.pg.osm.GeoLocated;
import no.hal.pg.osm.GeoLocation;
import no.hal.pg.osm.OsmPackage;
import no.hal.pg.osm.geoutil.LatLong;
import no.hal.pg.runtime.Player;
import no.hal.pg.runtime.impl.ResettableTaskImpl;
import tdt4250.gr1700.location.DistanceHint;
import tdt4250.gr1700.location.LocationPackage;
import tdt4250.gr1700.location.LocationTask;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.gr1700.location.impl.LocationTaskImpl#getLatitude <em>Latitude</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.impl.LocationTaskImpl#getLongitude <em>Longitude</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.impl.LocationTaskImpl#getTargetDistance <em>Target Distance</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.impl.LocationTaskImpl#getHints <em>Hints</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.impl.LocationTaskImpl#getAttemptCount <em>Attempt Count</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.impl.LocationTaskImpl#getPlayer <em>Player</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LocationTaskImpl extends ResettableTaskImpl<Double> implements LocationTask {
	/**
	 * The default value of the '{@link #getLatitude() <em>Latitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLatitude()
	 * @generated
	 * @ordered
	 */
	protected static final float LATITUDE_EDEFAULT = 0.0F;
	/**
	 * The cached value of the '{@link #getLatitude() <em>Latitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLatitude()
	 * @generated
	 * @ordered
	 */
	protected float latitude = LATITUDE_EDEFAULT;
	/**
	 * The default value of the '{@link #getLongitude() <em>Longitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLongitude()
	 * @generated
	 * @ordered
	 */
	protected static final float LONGITUDE_EDEFAULT = 0.0F;
	/**
	 * The cached value of the '{@link #getLongitude() <em>Longitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLongitude()
	 * @generated
	 * @ordered
	 */
	protected float longitude = LONGITUDE_EDEFAULT;
	/**
	 * The default value of the '{@link #getTargetDistance() <em>Target Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetDistance()
	 * @generated
	 * @ordered
	 */
	protected static final int TARGET_DISTANCE_EDEFAULT = 10;
	/**
	 * The cached value of the '{@link #getTargetDistance() <em>Target Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetDistance()
	 * @generated
	 * @ordered
	 */
	protected int targetDistance = TARGET_DISTANCE_EDEFAULT;
	/**
	 * The cached value of the '{@link #getHints() <em>Hints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHints()
	 * @generated
	 * @ordered
	 */
	protected EList<DistanceHint> hints;
	/**
	 * The default value of the '{@link #getAttemptCount() <em>Attempt Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttemptCount()
	 * @generated
	 * @ordered
	 */
	protected static final int ATTEMPT_COUNT_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getAttemptCount() <em>Attempt Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttemptCount()
	 * @generated
	 * @ordered
	 */
	protected int attemptCount = ATTEMPT_COUNT_EDEFAULT;
	/**
	 * The cached value of the '{@link #getPlayer() <em>Player</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlayer()
	 * @generated
	 * @ordered
	 */
	protected Player player;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocationTaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LocationPackage.Literals.LOCATION_TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public float getLatitude() {
		return latitude;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLatitude(float newLatitude) {
		float oldLatitude = latitude;
		latitude = newLatitude;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LocationPackage.LOCATION_TASK__LATITUDE, oldLatitude, latitude));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public float getLongitude() {
		return longitude;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLongitude(float newLongitude) {
		float oldLongitude = longitude;
		longitude = newLongitude;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LocationPackage.LOCATION_TASK__LONGITUDE, oldLongitude, longitude));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getTargetDistance() {
		return targetDistance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTargetDistance(int newTargetDistance) {
		int oldTargetDistance = targetDistance;
		targetDistance = newTargetDistance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LocationPackage.LOCATION_TASK__TARGET_DISTANCE, oldTargetDistance, targetDistance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Player getPlayer() {
		if (player != null && player.eIsProxy()) {
			InternalEObject oldPlayer = (InternalEObject)player;
			player = (Player)eResolveProxy(oldPlayer);
			if (player != oldPlayer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LocationPackage.LOCATION_TASK__PLAYER, oldPlayer, player));
			}
		}
		return player;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Player basicGetPlayer() {
		return player;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPlayer(Player newPlayer) {
		Player oldPlayer = player;
		player = newPlayer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LocationPackage.LOCATION_TASK__PLAYER, oldPlayer, player));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DistanceHint> getHints() {
		if (hints == null) {
			hints = new EObjectContainmentEList<DistanceHint>(DistanceHint.class, this, LocationPackage.LOCATION_TASK__HINTS);
		}
		return hints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAttemptCount() {
		return attemptCount;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttemptCount(int newAttemptCount) {
		int oldAttemptCount = attemptCount;
		attemptCount = newAttemptCount;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LocationPackage.LOCATION_TASK__ATTEMPT_COUNT, oldAttemptCount, attemptCount));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean checkPlayerLocation(Player player) {
		if (! getPlayers().contains(player)) {
			throw new IllegalArgumentException("Player must be one of this task's players");
		}
		setAttemptCount(getAttemptCount() + 1);
		double distance = getTargetDistance(player);
		if (distance < getTargetDistance()) {
			if (getResult() == null || distance < getResult()) {
				setPlayer(player);
				finish(distance);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double getTargetDistance(GeoLocated geoLocated) {
		LatLong target = getLatLong();
		LatLong latLong = geoLocated.getLatLong();
		return LatLong.distance(target.latitude, target.longitude, latLong.latitude, latLong.longitude);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getHint(double distance) {
		double leastDistance = -1;
		DistanceHint closestHint = null;
		for (DistanceHint hint : getHints()) {
			if (hint.getDistance() < 0 || distance < hint.getDistance()) {
				// where close enough for this hint
				if (leastDistance < 0 || (hint.getDistance() >= 0 && hint.getDistance() < leastDistance)) {
					// this hint is better than the current one
					leastDistance = hint.getDistance();
					closestHint = hint;
				}
			}
		}
		return (closestHint != null ? closestHint.getHint() : null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public LatLong getLatLong() {
		return new LatLong(getLatitude(), getLongitude());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LocationPackage.LOCATION_TASK__HINTS:
				return ((InternalEList<?>)getHints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LocationPackage.LOCATION_TASK__LATITUDE:
				return getLatitude();
			case LocationPackage.LOCATION_TASK__LONGITUDE:
				return getLongitude();
			case LocationPackage.LOCATION_TASK__TARGET_DISTANCE:
				return getTargetDistance();
			case LocationPackage.LOCATION_TASK__HINTS:
				return getHints();
			case LocationPackage.LOCATION_TASK__ATTEMPT_COUNT:
				return getAttemptCount();
			case LocationPackage.LOCATION_TASK__PLAYER:
				if (resolve) return getPlayer();
				return basicGetPlayer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LocationPackage.LOCATION_TASK__LATITUDE:
				setLatitude((Float)newValue);
				return;
			case LocationPackage.LOCATION_TASK__LONGITUDE:
				setLongitude((Float)newValue);
				return;
			case LocationPackage.LOCATION_TASK__TARGET_DISTANCE:
				setTargetDistance((Integer)newValue);
				return;
			case LocationPackage.LOCATION_TASK__HINTS:
				getHints().clear();
				getHints().addAll((Collection<? extends DistanceHint>)newValue);
				return;
			case LocationPackage.LOCATION_TASK__ATTEMPT_COUNT:
				setAttemptCount((Integer)newValue);
				return;
			case LocationPackage.LOCATION_TASK__PLAYER:
				setPlayer((Player)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LocationPackage.LOCATION_TASK__LATITUDE:
				setLatitude(LATITUDE_EDEFAULT);
				return;
			case LocationPackage.LOCATION_TASK__LONGITUDE:
				setLongitude(LONGITUDE_EDEFAULT);
				return;
			case LocationPackage.LOCATION_TASK__TARGET_DISTANCE:
				setTargetDistance(TARGET_DISTANCE_EDEFAULT);
				return;
			case LocationPackage.LOCATION_TASK__HINTS:
				getHints().clear();
				return;
			case LocationPackage.LOCATION_TASK__ATTEMPT_COUNT:
				setAttemptCount(ATTEMPT_COUNT_EDEFAULT);
				return;
			case LocationPackage.LOCATION_TASK__PLAYER:
				setPlayer((Player)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LocationPackage.LOCATION_TASK__LATITUDE:
				return latitude != LATITUDE_EDEFAULT;
			case LocationPackage.LOCATION_TASK__LONGITUDE:
				return longitude != LONGITUDE_EDEFAULT;
			case LocationPackage.LOCATION_TASK__TARGET_DISTANCE:
				return targetDistance != TARGET_DISTANCE_EDEFAULT;
			case LocationPackage.LOCATION_TASK__HINTS:
				return hints != null && !hints.isEmpty();
			case LocationPackage.LOCATION_TASK__ATTEMPT_COUNT:
				return attemptCount != ATTEMPT_COUNT_EDEFAULT;
			case LocationPackage.LOCATION_TASK__PLAYER:
				return player != null;
		}
		return super.eIsSet(featureID);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == GeoLocated.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == GeoLocation.class) {
			switch (derivedFeatureID) {
				case LocationPackage.LOCATION_TASK__LATITUDE: return OsmPackage.GEO_LOCATION__LATITUDE;
				case LocationPackage.LOCATION_TASK__LONGITUDE: return OsmPackage.GEO_LOCATION__LONGITUDE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == GeoLocated.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == GeoLocation.class) {
			switch (baseFeatureID) {
				case OsmPackage.GEO_LOCATION__LATITUDE: return LocationPackage.LOCATION_TASK__LATITUDE;
				case OsmPackage.GEO_LOCATION__LONGITUDE: return LocationPackage.LOCATION_TASK__LONGITUDE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == GeoLocated.class) {
			switch (baseOperationID) {
				case OsmPackage.GEO_LOCATED___GET_LAT_LONG: return LocationPackage.LOCATION_TASK___GET_LAT_LONG;
				default: return -1;
			}
		}
		if (baseClass == GeoLocation.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case LocationPackage.LOCATION_TASK___CHECK_PLAYER_LOCATION__PLAYER:
				return checkPlayerLocation((Player)arguments.get(0));
			case LocationPackage.LOCATION_TASK___GET_TARGET_DISTANCE__GEOLOCATED:
				return getTargetDistance((GeoLocated)arguments.get(0));
			case LocationPackage.LOCATION_TASK___GET_HINT__DOUBLE:
				return getHint((Double)arguments.get(0));
			case LocationPackage.LOCATION_TASK___GET_LAT_LONG:
				return getLatLong();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (latitude: ");
		result.append(latitude);
		result.append(", longitude: ");
		result.append(longitude);
		result.append(", targetDistance: ");
		result.append(targetDistance);
		result.append(", attemptCount: ");
		result.append(attemptCount);
		result.append(')');
		return result.toString();
	}

	//

	@Override
	public void reset() {
		setAttemptCount(0);
		setPlayer(null);
		super.reset();
	}

} //LocationTaskImpl
