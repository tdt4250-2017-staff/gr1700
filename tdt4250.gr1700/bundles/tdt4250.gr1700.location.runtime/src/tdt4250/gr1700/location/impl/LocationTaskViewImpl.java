/**
 */
package tdt4250.gr1700.location.impl;

import java.lang.reflect.InvocationTargetException;

import no.hal.pg.app.AppFactory;
import no.hal.pg.app.MapView;
import no.hal.pg.app.impl.TaskViewImpl;
import no.hal.pg.osm.geoutil.LatLong;
import no.hal.pg.runtime.Player;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import tdt4250.gr1700.location.LocationPackage;
import tdt4250.gr1700.location.LocationTask;
import tdt4250.gr1700.location.LocationTaskView;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.gr1700.location.impl.LocationTaskViewImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.impl.LocationTaskViewImpl#getHint <em>Hint</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.impl.LocationTaskViewImpl#getMapView <em>Map View</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LocationTaskViewImpl extends TaskViewImpl<LocationTask> implements LocationTaskView {
	/**
	 * The default value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected static final LatLong LOCATION_EDEFAULT = null;
	/**
	 * The default value of the '{@link #getHint() <em>Hint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHint()
	 * @generated
	 * @ordered
	 */
	protected static final String HINT_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getMapView() <em>Map View</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMapView()
	 * @generated
	 * @ordered
	 */
	protected MapView mapView;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocationTaskViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LocationPackage.Literals.LOCATION_TASK_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public LatLong getLocation() {
		return getUser().getLatLong();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getHint() {
		LocationTask task = getModel();
		double distance = task.getTargetDistance(getUser());
		return task.getHint(distance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public MapView getMapView() {
		if (mapView == null) {
			MapView mapView = AppFactory.eINSTANCE.createMapView();
			mapView.navigate(getUser().getLatitude(), getUser().getLongitude(), 12);
			setMapView(mapView);
		}
		return mapView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMapView(MapView newMapView, NotificationChain msgs) {
		MapView oldMapView = mapView;
		mapView = newMapView;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LocationPackage.LOCATION_TASK_VIEW__MAP_VIEW, oldMapView, newMapView);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMapView(MapView newMapView) {
		if (newMapView != mapView) {
			NotificationChain msgs = null;
			if (mapView != null)
				msgs = ((InternalEObject)mapView).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - LocationPackage.LOCATION_TASK_VIEW__MAP_VIEW, null, msgs);
			if (newMapView != null)
				msgs = ((InternalEObject)newMapView).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - LocationPackage.LOCATION_TASK_VIEW__MAP_VIEW, null, msgs);
			msgs = basicSetMapView(newMapView, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LocationPackage.LOCATION_TASK_VIEW__MAP_VIEW, newMapView, newMapView));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public LocationTaskView setPlayerLocation(float latitude, float longitude) {
		Player player = getUser();
		player.setLatitude(latitude);
		player.setLongitude(longitude);
		return this;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean tryFinish() {
		return getModel().checkPlayerLocation(getUser());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LocationPackage.LOCATION_TASK_VIEW__MAP_VIEW:
				return basicSetMapView(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LocationPackage.LOCATION_TASK_VIEW__LOCATION:
				return getLocation();
			case LocationPackage.LOCATION_TASK_VIEW__HINT:
				return getHint();
			case LocationPackage.LOCATION_TASK_VIEW__MAP_VIEW:
				return getMapView();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LocationPackage.LOCATION_TASK_VIEW__MAP_VIEW:
				setMapView((MapView)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LocationPackage.LOCATION_TASK_VIEW__MAP_VIEW:
				setMapView((MapView)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LocationPackage.LOCATION_TASK_VIEW__LOCATION:
				return LOCATION_EDEFAULT == null ? getLocation() != null : !LOCATION_EDEFAULT.equals(getLocation());
			case LocationPackage.LOCATION_TASK_VIEW__HINT:
				return HINT_EDEFAULT == null ? getHint() != null : !HINT_EDEFAULT.equals(getHint());
			case LocationPackage.LOCATION_TASK_VIEW__MAP_VIEW:
				return mapView != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case LocationPackage.LOCATION_TASK_VIEW___SET_PLAYER_LOCATION__FLOAT_FLOAT:
				return setPlayerLocation((Float)arguments.get(0), (Float)arguments.get(1));
			case LocationPackage.LOCATION_TASK_VIEW___TRY_FINISH:
				return tryFinish();
		}
		return super.eInvoke(operationID, arguments);
	}

} //LocationTaskViewImpl
