/**
 */
package tdt4250.gr1700.location;

import no.hal.pg.app.AppPackage;
import no.hal.pg.runtime.RuntimePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see tdt4250.gr1700.location.LocationFactory
 * @model kind="package"
 * @generated
 */
public interface LocationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "location";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/tdt4250.gr1700.location.runtime/model/location-runtime.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "location";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LocationPackage eINSTANCE = tdt4250.gr1700.location.impl.LocationPackageImpl.init();

	/**
	 * The meta object id for the '{@link tdt4250.gr1700.location.impl.LocationTaskImpl <em>Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.gr1700.location.impl.LocationTaskImpl
	 * @see tdt4250.gr1700.location.impl.LocationPackageImpl#getLocationTask()
	 * @generated
	 */
	int LOCATION_TASK = 0;

	/**
	 * The feature id for the '<em><b>Game</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__GAME = RuntimePackage.RESETTABLE_TASK__GAME;

	/**
	 * The feature id for the '<em><b>Players</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__PLAYERS = RuntimePackage.RESETTABLE_TASK__PLAYERS;

	/**
	 * The feature id for the '<em><b>Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__RESULT = RuntimePackage.RESETTABLE_TASK__RESULT;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__START_TIME = RuntimePackage.RESETTABLE_TASK__START_TIME;

	/**
	 * The feature id for the '<em><b>Finish Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__FINISH_TIME = RuntimePackage.RESETTABLE_TASK__FINISH_TIME;

	/**
	 * The feature id for the '<em><b>Start Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__START_CONDITIONS = RuntimePackage.RESETTABLE_TASK__START_CONDITIONS;

	/**
	 * The feature id for the '<em><b>Finish Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__FINISH_CONDITIONS = RuntimePackage.RESETTABLE_TASK__FINISH_CONDITIONS;

	/**
	 * The feature id for the '<em><b>Start Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__START_ACTIONS = RuntimePackage.RESETTABLE_TASK__START_ACTIONS;

	/**
	 * The feature id for the '<em><b>Finish Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__FINISH_ACTIONS = RuntimePackage.RESETTABLE_TASK__FINISH_ACTIONS;

	/**
	 * The feature id for the '<em><b>Latitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__LATITUDE = RuntimePackage.RESETTABLE_TASK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Longitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__LONGITUDE = RuntimePackage.RESETTABLE_TASK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target Distance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__TARGET_DISTANCE = RuntimePackage.RESETTABLE_TASK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Hints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__HINTS = RuntimePackage.RESETTABLE_TASK_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Attempt Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__ATTEMPT_COUNT = RuntimePackage.RESETTABLE_TASK_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Player</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK__PLAYER = RuntimePackage.RESETTABLE_TASK_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_FEATURE_COUNT = RuntimePackage.RESETTABLE_TASK_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK___GET_DESCRIPTION = RuntimePackage.RESETTABLE_TASK___GET_DESCRIPTION;

	/**
	 * The operation id for the '<em>Can Start</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK___CAN_START = RuntimePackage.RESETTABLE_TASK___CAN_START;

	/**
	 * The operation id for the '<em>Is Started</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK___IS_STARTED = RuntimePackage.RESETTABLE_TASK___IS_STARTED;

	/**
	 * The operation id for the '<em>Is Finished</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK___IS_FINISHED = RuntimePackage.RESETTABLE_TASK___IS_FINISHED;

	/**
	 * The operation id for the '<em>Start</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK___START = RuntimePackage.RESETTABLE_TASK___START;

	/**
	 * The operation id for the '<em>Finish</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK___FINISH__OBJECT = RuntimePackage.RESETTABLE_TASK___FINISH__OBJECT;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK___RESET = RuntimePackage.RESETTABLE_TASK___RESET;

	/**
	 * The operation id for the '<em>Get Lat Long</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK___GET_LAT_LONG = RuntimePackage.RESETTABLE_TASK_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Check Player Location</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK___CHECK_PLAYER_LOCATION__PLAYER = RuntimePackage.RESETTABLE_TASK_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Target Distance</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK___GET_TARGET_DISTANCE__GEOLOCATED = RuntimePackage.RESETTABLE_TASK_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Hint</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK___GET_HINT__DOUBLE = RuntimePackage.RESETTABLE_TASK_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_OPERATION_COUNT = RuntimePackage.RESETTABLE_TASK_OPERATION_COUNT + 4;


	/**
	 * The meta object id for the '{@link tdt4250.gr1700.location.impl.DistanceHintImpl <em>Distance Hint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.gr1700.location.impl.DistanceHintImpl
	 * @see tdt4250.gr1700.location.impl.LocationPackageImpl#getDistanceHint()
	 * @generated
	 */
	int DISTANCE_HINT = 1;

	/**
	 * The feature id for the '<em><b>Distance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISTANCE_HINT__DISTANCE = 0;

	/**
	 * The feature id for the '<em><b>Hint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISTANCE_HINT__HINT = 1;

	/**
	 * The number of structural features of the '<em>Distance Hint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISTANCE_HINT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Distance Hint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISTANCE_HINT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.gr1700.location.impl.LocationTaskViewImpl <em>Task View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.gr1700.location.impl.LocationTaskViewImpl
	 * @see tdt4250.gr1700.location.impl.LocationPackageImpl#getLocationTaskView()
	 * @generated
	 */
	int LOCATION_TASK_VIEW = 2;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW__USER = AppPackage.TASK_VIEW__USER;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW__MODEL = AppPackage.TASK_VIEW__MODEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW__DESCRIPTION = AppPackage.TASK_VIEW__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW__ENABLED = AppPackage.TASK_VIEW__ENABLED;

	/**
	 * The feature id for the '<em><b>Started</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW__STARTED = AppPackage.TASK_VIEW__STARTED;

	/**
	 * The feature id for the '<em><b>Finished</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW__FINISHED = AppPackage.TASK_VIEW__FINISHED;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW__LOCATION = AppPackage.TASK_VIEW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Hint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW__HINT = AppPackage.TASK_VIEW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Map View</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW__MAP_VIEW = AppPackage.TASK_VIEW_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Task View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW_FEATURE_COUNT = AppPackage.TASK_VIEW_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Start</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW___START = AppPackage.TASK_VIEW___START;

	/**
	 * The operation id for the '<em>Set Player Location</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW___SET_PLAYER_LOCATION__FLOAT_FLOAT = AppPackage.TASK_VIEW_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Try Finish</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW___TRY_FINISH = AppPackage.TASK_VIEW_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Task View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_VIEW_OPERATION_COUNT = AppPackage.TASK_VIEW_OPERATION_COUNT + 2;


	/**
	 * Returns the meta object for class '{@link tdt4250.gr1700.location.LocationTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task</em>'.
	 * @see tdt4250.gr1700.location.LocationTask
	 * @generated
	 */
	EClass getLocationTask();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.gr1700.location.LocationTask#getTargetDistance <em>Target Distance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Distance</em>'.
	 * @see tdt4250.gr1700.location.LocationTask#getTargetDistance()
	 * @see #getLocationTask()
	 * @generated
	 */
	EAttribute getLocationTask_TargetDistance();

	/**
	 * Returns the meta object for the reference '{@link tdt4250.gr1700.location.LocationTask#getPlayer <em>Player</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Player</em>'.
	 * @see tdt4250.gr1700.location.LocationTask#getPlayer()
	 * @see #getLocationTask()
	 * @generated
	 */
	EReference getLocationTask_Player();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.gr1700.location.LocationTask#getHints <em>Hints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hints</em>'.
	 * @see tdt4250.gr1700.location.LocationTask#getHints()
	 * @see #getLocationTask()
	 * @generated
	 */
	EReference getLocationTask_Hints();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.gr1700.location.LocationTask#getAttemptCount <em>Attempt Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attempt Count</em>'.
	 * @see tdt4250.gr1700.location.LocationTask#getAttemptCount()
	 * @see #getLocationTask()
	 * @generated
	 */
	EAttribute getLocationTask_AttemptCount();

	/**
	 * Returns the meta object for the '{@link tdt4250.gr1700.location.LocationTask#checkPlayerLocation(no.hal.pg.runtime.Player) <em>Check Player Location</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Player Location</em>' operation.
	 * @see tdt4250.gr1700.location.LocationTask#checkPlayerLocation(no.hal.pg.runtime.Player)
	 * @generated
	 */
	EOperation getLocationTask__CheckPlayerLocation__Player();

	/**
	 * Returns the meta object for the '{@link tdt4250.gr1700.location.LocationTask#getTargetDistance(no.hal.pg.osm.GeoLocated) <em>Get Target Distance</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Target Distance</em>' operation.
	 * @see tdt4250.gr1700.location.LocationTask#getTargetDistance(no.hal.pg.osm.GeoLocated)
	 * @generated
	 */
	EOperation getLocationTask__GetTargetDistance__GeoLocated();

	/**
	 * Returns the meta object for the '{@link tdt4250.gr1700.location.LocationTask#getHint(double) <em>Get Hint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Hint</em>' operation.
	 * @see tdt4250.gr1700.location.LocationTask#getHint(double)
	 * @generated
	 */
	EOperation getLocationTask__GetHint__double();

	/**
	 * Returns the meta object for class '{@link tdt4250.gr1700.location.DistanceHint <em>Distance Hint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Distance Hint</em>'.
	 * @see tdt4250.gr1700.location.DistanceHint
	 * @generated
	 */
	EClass getDistanceHint();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.gr1700.location.DistanceHint#getDistance <em>Distance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Distance</em>'.
	 * @see tdt4250.gr1700.location.DistanceHint#getDistance()
	 * @see #getDistanceHint()
	 * @generated
	 */
	EAttribute getDistanceHint_Distance();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.gr1700.location.DistanceHint#getHint <em>Hint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hint</em>'.
	 * @see tdt4250.gr1700.location.DistanceHint#getHint()
	 * @see #getDistanceHint()
	 * @generated
	 */
	EAttribute getDistanceHint_Hint();

	/**
	 * Returns the meta object for class '{@link tdt4250.gr1700.location.LocationTaskView <em>Task View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task View</em>'.
	 * @see tdt4250.gr1700.location.LocationTaskView
	 * @generated
	 */
	EClass getLocationTaskView();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.gr1700.location.LocationTaskView#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see tdt4250.gr1700.location.LocationTaskView#getLocation()
	 * @see #getLocationTaskView()
	 * @generated
	 */
	EAttribute getLocationTaskView_Location();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.gr1700.location.LocationTaskView#getHint <em>Hint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hint</em>'.
	 * @see tdt4250.gr1700.location.LocationTaskView#getHint()
	 * @see #getLocationTaskView()
	 * @generated
	 */
	EAttribute getLocationTaskView_Hint();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.gr1700.location.LocationTaskView#getMapView <em>Map View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Map View</em>'.
	 * @see tdt4250.gr1700.location.LocationTaskView#getMapView()
	 * @see #getLocationTaskView()
	 * @generated
	 */
	EReference getLocationTaskView_MapView();

	/**
	 * Returns the meta object for the '{@link tdt4250.gr1700.location.LocationTaskView#setPlayerLocation(float, float) <em>Set Player Location</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Player Location</em>' operation.
	 * @see tdt4250.gr1700.location.LocationTaskView#setPlayerLocation(float, float)
	 * @generated
	 */
	EOperation getLocationTaskView__SetPlayerLocation__float_float();

	/**
	 * Returns the meta object for the '{@link tdt4250.gr1700.location.LocationTaskView#tryFinish() <em>Try Finish</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Try Finish</em>' operation.
	 * @see tdt4250.gr1700.location.LocationTaskView#tryFinish()
	 * @generated
	 */
	EOperation getLocationTaskView__TryFinish();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	LocationFactory getLocationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link tdt4250.gr1700.location.impl.LocationTaskImpl <em>Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.gr1700.location.impl.LocationTaskImpl
		 * @see tdt4250.gr1700.location.impl.LocationPackageImpl#getLocationTask()
		 * @generated
		 */
		EClass LOCATION_TASK = eINSTANCE.getLocationTask();

		/**
		 * The meta object literal for the '<em><b>Target Distance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCATION_TASK__TARGET_DISTANCE = eINSTANCE.getLocationTask_TargetDistance();

		/**
		 * The meta object literal for the '<em><b>Player</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCATION_TASK__PLAYER = eINSTANCE.getLocationTask_Player();

		/**
		 * The meta object literal for the '<em><b>Hints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCATION_TASK__HINTS = eINSTANCE.getLocationTask_Hints();

		/**
		 * The meta object literal for the '<em><b>Attempt Count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCATION_TASK__ATTEMPT_COUNT = eINSTANCE.getLocationTask_AttemptCount();

		/**
		 * The meta object literal for the '<em><b>Check Player Location</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOCATION_TASK___CHECK_PLAYER_LOCATION__PLAYER = eINSTANCE.getLocationTask__CheckPlayerLocation__Player();

		/**
		 * The meta object literal for the '<em><b>Get Target Distance</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOCATION_TASK___GET_TARGET_DISTANCE__GEOLOCATED = eINSTANCE.getLocationTask__GetTargetDistance__GeoLocated();

		/**
		 * The meta object literal for the '<em><b>Get Hint</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOCATION_TASK___GET_HINT__DOUBLE = eINSTANCE.getLocationTask__GetHint__double();

		/**
		 * The meta object literal for the '{@link tdt4250.gr1700.location.impl.DistanceHintImpl <em>Distance Hint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.gr1700.location.impl.DistanceHintImpl
		 * @see tdt4250.gr1700.location.impl.LocationPackageImpl#getDistanceHint()
		 * @generated
		 */
		EClass DISTANCE_HINT = eINSTANCE.getDistanceHint();

		/**
		 * The meta object literal for the '<em><b>Distance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISTANCE_HINT__DISTANCE = eINSTANCE.getDistanceHint_Distance();

		/**
		 * The meta object literal for the '<em><b>Hint</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISTANCE_HINT__HINT = eINSTANCE.getDistanceHint_Hint();

		/**
		 * The meta object literal for the '{@link tdt4250.gr1700.location.impl.LocationTaskViewImpl <em>Task View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.gr1700.location.impl.LocationTaskViewImpl
		 * @see tdt4250.gr1700.location.impl.LocationPackageImpl#getLocationTaskView()
		 * @generated
		 */
		EClass LOCATION_TASK_VIEW = eINSTANCE.getLocationTaskView();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCATION_TASK_VIEW__LOCATION = eINSTANCE.getLocationTaskView_Location();

		/**
		 * The meta object literal for the '<em><b>Hint</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCATION_TASK_VIEW__HINT = eINSTANCE.getLocationTaskView_Hint();

		/**
		 * The meta object literal for the '<em><b>Map View</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCATION_TASK_VIEW__MAP_VIEW = eINSTANCE.getLocationTaskView_MapView();

		/**
		 * The meta object literal for the '<em><b>Set Player Location</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOCATION_TASK_VIEW___SET_PLAYER_LOCATION__FLOAT_FLOAT = eINSTANCE.getLocationTaskView__SetPlayerLocation__float_float();

		/**
		 * The meta object literal for the '<em><b>Try Finish</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOCATION_TASK_VIEW___TRY_FINISH = eINSTANCE.getLocationTaskView__TryFinish();

	}

} //LocationPackage
