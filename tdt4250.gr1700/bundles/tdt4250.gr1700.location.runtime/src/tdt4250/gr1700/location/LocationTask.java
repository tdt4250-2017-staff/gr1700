/**
 */
package tdt4250.gr1700.location;

import no.hal.pg.osm.GeoLocated;
import no.hal.pg.osm.GeoLocation;
import no.hal.pg.runtime.Player;
import no.hal.pg.runtime.ResettableTask;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.gr1700.location.LocationTask#getTargetDistance <em>Target Distance</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.LocationTask#getHints <em>Hints</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.LocationTask#getAttemptCount <em>Attempt Count</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.LocationTask#getPlayer <em>Player</em>}</li>
 * </ul>
 *
 * @see tdt4250.gr1700.location.LocationPackage#getLocationTask()
 * @model superTypes="no.hal.pg.runtime.ResettableTask&lt;org.eclipse.emf.ecore.EDoubleObject&gt; no.hal.pg.osm.GeoLocation"
 * @generated
 */
public interface LocationTask extends ResettableTask<Double>, GeoLocation {
	/**
	 * Returns the value of the '<em><b>Target Distance</b></em>' attribute.
	 * The default value is <code>"10"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Distance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Distance</em>' attribute.
	 * @see #setTargetDistance(int)
	 * @see tdt4250.gr1700.location.LocationPackage#getLocationTask_TargetDistance()
	 * @model default="10"
	 * @generated
	 */
	int getTargetDistance();

	/**
	 * Sets the value of the '{@link tdt4250.gr1700.location.LocationTask#getTargetDistance <em>Target Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Distance</em>' attribute.
	 * @see #getTargetDistance()
	 * @generated
	 */
	void setTargetDistance(int value);

	/**
	 * Returns the value of the '<em><b>Player</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Player</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Player</em>' reference.
	 * @see #setPlayer(Player)
	 * @see tdt4250.gr1700.location.LocationPackage#getLocationTask_Player()
	 * @model
	 * @generated
	 */
	Player getPlayer();

	/**
	 * Sets the value of the '{@link tdt4250.gr1700.location.LocationTask#getPlayer <em>Player</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Player</em>' reference.
	 * @see #getPlayer()
	 * @generated
	 */
	void setPlayer(Player value);

	/**
	 * Returns the value of the '<em><b>Hints</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.gr1700.location.DistanceHint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hints</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hints</em>' containment reference list.
	 * @see tdt4250.gr1700.location.LocationPackage#getLocationTask_Hints()
	 * @model containment="true"
	 * @generated
	 */
	EList<DistanceHint> getHints();

	/**
	 * Returns the value of the '<em><b>Attempt Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attempt Count</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attempt Count</em>' attribute.
	 * @see #setAttemptCount(int)
	 * @see tdt4250.gr1700.location.LocationPackage#getLocationTask_AttemptCount()
	 * @model
	 * @generated
	 */
	int getAttemptCount();

	/**
	 * Sets the value of the '{@link tdt4250.gr1700.location.LocationTask#getAttemptCount <em>Attempt Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attempt Count</em>' attribute.
	 * @see #getAttemptCount()
	 * @generated
	 */
	void setAttemptCount(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkPlayerLocation(Player player);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	double getTargetDistance(GeoLocated geoLocated);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	String getHint(double distance);

} // LocationTask
