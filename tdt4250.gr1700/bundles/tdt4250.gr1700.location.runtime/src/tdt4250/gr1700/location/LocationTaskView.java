/**
 */
package tdt4250.gr1700.location;

import no.hal.pg.app.MapView;
import no.hal.pg.app.TaskView;
import no.hal.pg.osm.geoutil.LatLong;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.gr1700.location.LocationTaskView#getLocation <em>Location</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.LocationTaskView#getHint <em>Hint</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.LocationTaskView#getMapView <em>Map View</em>}</li>
 * </ul>
 *
 * @see tdt4250.gr1700.location.LocationPackage#getLocationTaskView()
 * @model
 * @generated
 */
public interface LocationTaskView extends TaskView<LocationTask> {

	/**
	 * Returns the value of the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' attribute.
	 * @see tdt4250.gr1700.location.LocationPackage#getLocationTaskView_Location()
	 * @model dataType="no.hal.pg.osm.LatLong" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	LatLong getLocation();

	/**
	 * Returns the value of the '<em><b>Hint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hint</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hint</em>' attribute.
	 * @see tdt4250.gr1700.location.LocationPackage#getLocationTaskView_Hint()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getHint();

	/**
	 * Returns the value of the '<em><b>Map View</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Map View</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Map View</em>' containment reference.
	 * @see #setMapView(MapView)
	 * @see tdt4250.gr1700.location.LocationPackage#getLocationTaskView_MapView()
	 * @model containment="true"
	 * @generated
	 */
	MapView getMapView();

	/**
	 * Sets the value of the '{@link tdt4250.gr1700.location.LocationTaskView#getMapView <em>Map View</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Map View</em>' containment reference.
	 * @see #getMapView()
	 * @generated
	 */
	void setMapView(MapView value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	LocationTaskView setPlayerLocation(float latitude, float longitude);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean tryFinish();
} // LocationTaskView
