package tdt4250.gr1700.location.util;

import org.eclipse.emf.ecore.EObject;
import org.osgi.service.component.annotations.Component;

import no.hal.pg.app.View;
import no.hal.pg.app.util.IViewFactory;
import tdt4250.gr1700.location.LocationFactory;
import tdt4250.gr1700.location.LocationTask;

@Component
public class LocationTaskViewFactory implements IViewFactory {

	@Override
	public View<?> createView(EObject user, EObject eObject) {
		if (eObject instanceof LocationTask) {
			return LocationFactory.eINSTANCE.createLocationTaskView();
		}
		return null;
	}
}
