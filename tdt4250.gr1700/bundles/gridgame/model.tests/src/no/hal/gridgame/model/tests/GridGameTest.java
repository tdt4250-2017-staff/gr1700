package no.hal.gridgame.model.tests;

import org.junit.Assert;
import org.junit.Test;

import no.hal.gridgame.model.GridChangeDescription;
import no.hal.gridgame.model.GridRectangle;
import no.hal.gridgame.model.ModelFactory;

public class GridGameTest {

	private void checkGridRectangle(GridRectangle gridRectangle, int x, int y, int w, int h) {
		Assert.assertEquals(x, gridRectangle.getX());
		Assert.assertEquals(y, gridRectangle.getY());
		Assert.assertEquals(w, gridRectangle.getWidth());
		Assert.assertEquals(h, gridRectangle.getHeight());
	}

	@Test
	public void testSetValues__int_int() {
		GridRectangle gridRectangle = ModelFactory.eINSTANCE.createGridRectangle();
		gridRectangle.setValues(4, 5);
		checkGridRectangle(gridRectangle, 4, 5, 0, 0);
	}

	@Test
	public void testSetValues__int_int_int_int() {
		GridRectangle gridRectangle = ModelFactory.eINSTANCE.createGridRectangle();
		gridRectangle.setValues(4, 5, 3, 1);
		checkGridRectangle(gridRectangle, 4, 5, 3, 1);
	}

	@Test
	public void testSetValues__GridRectangle() {
		GridRectangle gridRectangle1 = ModelFactory.eINSTANCE.createGridRectangle();
		gridRectangle1.setValues(4, 5, 3, 1);
		GridRectangle gridRectangle2 = ModelFactory.eINSTANCE.createGridRectangle();
		gridRectangle2.setValues(gridRectangle1);
		checkGridRectangle(gridRectangle2, 4, 5, 3, 1);
	}

	@Test
	public void testAddChange__int_int() {
		GridChangeDescription changeDescription = ModelFactory.eINSTANCE.createGridChangeDescription();
		changeDescription.addChange(2, 3);
		checkGridRectangle(changeDescription, 2, 3, 1, 1);
		changeDescription.addChange(3, 3);
		checkGridRectangle(changeDescription, 2, 3, 2, 1);
	}

	@Test
	public void testAddChange__int_int_int_int() {
		GridChangeDescription changeDescription = ModelFactory.eINSTANCE.createGridChangeDescription();
		changeDescription.addChange(2, 3, 2, 1);
		checkGridRectangle(changeDescription, 2, 3, 2, 1);
		changeDescription.addChange(3, 3, 3, 4);
		checkGridRectangle(changeDescription, 2, 3, 4, 4);
	}
}
