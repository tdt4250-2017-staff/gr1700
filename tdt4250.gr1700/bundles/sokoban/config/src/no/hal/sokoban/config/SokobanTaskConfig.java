/**
 */
package no.hal.sokoban.config;

import no.hal.pg.config.TaskConfig;

import no.hal.sokoban.model.SokobanLevel;

import no.hal.sokoban.runtime.SokobanTask;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sokoban Task Config</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.hal.sokoban.config.SokobanTaskConfig#getLevel <em>Level</em>}</li>
 * </ul>
 *
 * @see no.hal.sokoban.config.ConfigPackage#getSokobanTaskConfig()
 * @model
 * @generated
 */
public interface SokobanTaskConfig extends TaskConfig<SokobanTask> {
	/**
	 * Returns the value of the '<em><b>Level</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' reference.
	 * @see #setLevel(SokobanLevel)
	 * @see no.hal.sokoban.config.ConfigPackage#getSokobanTaskConfig_Level()
	 * @model
	 * @generated
	 */
	SokobanLevel getLevel();

	/**
	 * Sets the value of the '{@link no.hal.sokoban.config.SokobanTaskConfig#getLevel <em>Level</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level</em>' reference.
	 * @see #getLevel()
	 * @generated
	 */
	void setLevel(SokobanLevel value);

} // SokobanTaskConfig
