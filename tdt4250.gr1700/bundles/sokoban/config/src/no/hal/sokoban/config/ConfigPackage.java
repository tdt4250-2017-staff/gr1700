/**
 */
package no.hal.sokoban.config;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see no.hal.sokoban.config.ConfigFactory
 * @model kind="package"
 * @generated
 */
public interface ConfigPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "config";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/no.hal.sokoban.config/model/sokoban-config.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "sokoban-config";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConfigPackage eINSTANCE = no.hal.sokoban.config.impl.ConfigPackageImpl.init();

	/**
	 * The meta object id for the '{@link no.hal.sokoban.config.impl.SokobanTaskConfigImpl <em>Sokoban Task Config</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.hal.sokoban.config.impl.SokobanTaskConfigImpl
	 * @see no.hal.sokoban.config.impl.ConfigPackageImpl#getSokobanTaskConfig()
	 * @generated
	 */
	int SOKOBAN_TASK_CONFIG = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_CONFIG__NAME = no.hal.pg.config.ConfigPackage.TASK_CONFIG__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_CONFIG__DESCRIPTION = no.hal.pg.config.ConfigPackage.TASK_CONFIG__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Level</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_CONFIG__LEVEL = no.hal.pg.config.ConfigPackage.TASK_CONFIG_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sokoban Task Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_CONFIG_FEATURE_COUNT = no.hal.pg.config.ConfigPackage.TASK_CONFIG_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Task</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_CONFIG___CREATE_TASK__TASKPROXY = no.hal.pg.config.ConfigPackage.TASK_CONFIG___CREATE_TASK__TASKPROXY;

	/**
	 * The number of operations of the '<em>Sokoban Task Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_CONFIG_OPERATION_COUNT = no.hal.pg.config.ConfigPackage.TASK_CONFIG_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link no.hal.sokoban.config.SokobanTaskConfig <em>Sokoban Task Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sokoban Task Config</em>'.
	 * @see no.hal.sokoban.config.SokobanTaskConfig
	 * @generated
	 */
	EClass getSokobanTaskConfig();

	/**
	 * Returns the meta object for the reference '{@link no.hal.sokoban.config.SokobanTaskConfig#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Level</em>'.
	 * @see no.hal.sokoban.config.SokobanTaskConfig#getLevel()
	 * @see #getSokobanTaskConfig()
	 * @generated
	 */
	EReference getSokobanTaskConfig_Level();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ConfigFactory getConfigFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link no.hal.sokoban.config.impl.SokobanTaskConfigImpl <em>Sokoban Task Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.hal.sokoban.config.impl.SokobanTaskConfigImpl
		 * @see no.hal.sokoban.config.impl.ConfigPackageImpl#getSokobanTaskConfig()
		 * @generated
		 */
		EClass SOKOBAN_TASK_CONFIG = eINSTANCE.getSokobanTaskConfig();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOKOBAN_TASK_CONFIG__LEVEL = eINSTANCE.getSokobanTaskConfig_Level();

	}

} //ConfigPackage
