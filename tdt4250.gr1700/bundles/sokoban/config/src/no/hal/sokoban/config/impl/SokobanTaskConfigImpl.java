/**
 */
package no.hal.sokoban.config.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import no.hal.pg.config.TaskProxy;
import no.hal.pg.config.impl.TaskConfigImpl;
import no.hal.sokoban.config.ConfigPackage;
import no.hal.sokoban.config.SokobanTaskConfig;
import no.hal.sokoban.model.ModelFactory;
import no.hal.sokoban.model.SokobanGame;
import no.hal.sokoban.model.SokobanLevel;
import no.hal.sokoban.runtime.RuntimeFactory;
import no.hal.sokoban.runtime.SokobanTask;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sokoban Task Config</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link no.hal.sokoban.config.impl.SokobanTaskConfigImpl#getLevel <em>Level</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SokobanTaskConfigImpl extends TaskConfigImpl<SokobanTask> implements SokobanTaskConfig {
	/**
	 * The cached value of the '{@link #getLevel() <em>Level</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected SokobanLevel level;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SokobanTaskConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigPackage.Literals.SOKOBAN_TASK_CONFIG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SokobanLevel getLevel() {
		if (level != null && level.eIsProxy()) {
			InternalEObject oldLevel = (InternalEObject)level;
			level = (SokobanLevel)eResolveProxy(oldLevel);
			if (level != oldLevel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConfigPackage.SOKOBAN_TASK_CONFIG__LEVEL, oldLevel, level));
			}
		}
		return level;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SokobanLevel basicGetLevel() {
		return level;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLevel(SokobanLevel newLevel) {
		SokobanLevel oldLevel = level;
		level = newLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.SOKOBAN_TASK_CONFIG__LEVEL, oldLevel, level));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigPackage.SOKOBAN_TASK_CONFIG__LEVEL:
				if (resolve) return getLevel();
				return basicGetLevel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigPackage.SOKOBAN_TASK_CONFIG__LEVEL:
				setLevel((SokobanLevel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigPackage.SOKOBAN_TASK_CONFIG__LEVEL:
				setLevel((SokobanLevel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigPackage.SOKOBAN_TASK_CONFIG__LEVEL:
				return level != null;
		}
		return super.eIsSet(featureID);
	}

	//

	@Override
	public SokobanTask createTask(TaskProxy proxy) {
		SokobanTask sokobanTask = RuntimeFactory.eINSTANCE.createSokobanTask();
		SokobanGame sokobanGame = ModelFactory.eINSTANCE.createSokobanGame();
		SokobanLevel levelCopy = EcoreUtil.copy(getLevel());
		sokobanTask.setSokobanLevel(levelCopy);
		sokobanGame.setLevel(levelCopy);
		sokobanGame.setGrid(levelCopy.createGrid());
		sokobanTask.setSokobanGame(sokobanGame);
		return sokobanTask;
	}

} //SokobanTaskConfigImpl
