/**
 */
package no.hal.sokoban.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Sokoban Cell</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see no.hal.sokoban.model.ModelPackage#getCell()
 * @model
 * @generated
 */
public enum Cell implements Enumerator {
	/**
	 * The '<em><b>EMPTY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EMPTY_VALUE
	 * @generated
	 * @ordered
	 */
	EMPTY(0, "EMPTY", " "),

	/**
	 * The '<em><b>WALL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WALL_VALUE
	 * @generated
	 * @ordered
	 */
	WALL(1, "WALL", "#"),

	/**
	 * The '<em><b>TARGET</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TARGET_VALUE
	 * @generated
	 * @ordered
	 */
	TARGET(2, "TARGET", "."),

	/**
	 * The '<em><b>BOX</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BOX_VALUE
	 * @generated
	 * @ordered
	 */
	BOX(3, "BOX", "$"),

	/**
	 * The '<em><b>BOX ON TARGET</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BOX_ON_TARGET_VALUE
	 * @generated
	 * @ordered
	 */
	BOX_ON_TARGET(4, "BOX_ON_TARGET", "*"),

	/**
	 * The '<em><b>PLAYER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PLAYER_VALUE
	 * @generated
	 * @ordered
	 */
	PLAYER(5, "PLAYER", "@"),

	/**
	 * The '<em><b>PLAYER ON TARGET</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PLAYER_ON_TARGET_VALUE
	 * @generated
	 * @ordered
	 */
	PLAYER_ON_TARGET(6, "PLAYER_ON_TARGET", "+");

	/**
	 * The '<em><b>EMPTY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EMPTY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EMPTY
	 * @model literal=" "
	 * @generated
	 * @ordered
	 */
	public static final int EMPTY_VALUE = 0;

	/**
	 * The '<em><b>WALL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>WALL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WALL
	 * @model literal="#"
	 * @generated
	 * @ordered
	 */
	public static final int WALL_VALUE = 1;

	/**
	 * The '<em><b>TARGET</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TARGET</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TARGET
	 * @model literal="."
	 * @generated
	 * @ordered
	 */
	public static final int TARGET_VALUE = 2;

	/**
	 * The '<em><b>BOX</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BOX</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BOX
	 * @model literal="$"
	 * @generated
	 * @ordered
	 */
	public static final int BOX_VALUE = 3;

	/**
	 * The '<em><b>BOX ON TARGET</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BOX ON TARGET</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BOX_ON_TARGET
	 * @model literal="*"
	 * @generated
	 * @ordered
	 */
	public static final int BOX_ON_TARGET_VALUE = 4;

	/**
	 * The '<em><b>PLAYER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PLAYER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PLAYER
	 * @model literal="@"
	 * @generated
	 * @ordered
	 */
	public static final int PLAYER_VALUE = 5;

	/**
	 * The '<em><b>PLAYER ON TARGET</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PLAYER ON TARGET</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PLAYER_ON_TARGET
	 * @model literal="+"
	 * @generated
	 * @ordered
	 */
	public static final int PLAYER_ON_TARGET_VALUE = 6;

	/**
	 * An array of all the '<em><b>Cell</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final Cell[] VALUES_ARRAY =
		new Cell[] {
			EMPTY,
			WALL,
			TARGET,
			BOX,
			BOX_ON_TARGET,
			PLAYER,
			PLAYER_ON_TARGET,
		};

	/**
	 * A public read-only list of all the '<em><b>Cell</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<Cell> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Cell</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Cell get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Cell result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Cell</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Cell getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Cell result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Cell</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Cell get(int value) {
		switch (value) {
			case EMPTY_VALUE: return EMPTY;
			case WALL_VALUE: return WALL;
			case TARGET_VALUE: return TARGET;
			case BOX_VALUE: return BOX;
			case BOX_ON_TARGET_VALUE: return BOX_ON_TARGET;
			case PLAYER_VALUE: return PLAYER;
			case PLAYER_ON_TARGET_VALUE: return PLAYER_ON_TARGET;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Cell(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
	//
	
	public Cell backgroundOf() {
		switch (this) {
		case WALL : return WALL;
		case TARGET : case BOX_ON_TARGET : case PLAYER_ON_TARGET : return TARGET;
		default : return EMPTY;
		}
	}

	public Cell foregroundOf(Cell cell) {
		switch (this) {
		case WALL : return WALL;
		case BOX : case BOX_ON_TARGET : return BOX; 
		case PLAYER : case PLAYER_ON_TARGET : return PLAYER;
		default : return EMPTY;
		}
	}
	
	public static Cell valueOf(Cell background, Cell foreground) {
		switch (background) {
			case WALL : return WALL;
			case EMPTY : case PLAYER : case BOX :
				switch (foreground) {
				case BOX_ON_TARGET : return BOX;
				case PLAYER_ON_TARGET :	return PLAYER;
				case TARGET : return EMPTY;
				default : return foreground;
				}
			case TARGET : case PLAYER_ON_TARGET : case BOX_ON_TARGET :
				switch (foreground) {
				case BOX : case BOX_ON_TARGET : return BOX_ON_TARGET;
				case PLAYER : case PLAYER_ON_TARGET : return PLAYER_ON_TARGET;
				case EMPTY : return TARGET;
				default : return null;
				}
			default : return null;
		}
	}

	public static Cell valueOf(char background, char foreground) {
		return valueOf(valueOf(background), valueOf(foreground));
	}
	
	public static Cell valueOf(Cell background) {
		return valueOf(background, EMPTY);
	}

	private final static String ALT_EMPTY = "-_";

	public static Cell valueOf(char c) {
		String s = (ALT_EMPTY.indexOf(c) >= 0 ? EMPTY.literal : String.valueOf(c));
		return get(s);
	}

	public char toChar() {
		return literal.charAt(0);
	}
	
	public boolean isOccupied() {
		switch (this) {
			case WALL : case BOX : case BOX_ON_TARGET : case PLAYER : case PLAYER_ON_TARGET: return true;
			default : return false;
		}
	}
	
	public boolean isTarget() {
		switch (this) {
			case TARGET : case BOX_ON_TARGET : case PLAYER_ON_TARGET: return true;
			default : return false;
		}
	}

	public boolean isPlayer() {
		switch (this) {
			case PLAYER: case PLAYER_ON_TARGET: return true;
			default : return false;
		}
	}

	public boolean isBox() {
		switch (this) {
			case BOX: case BOX_ON_TARGET: return true;
			default : return false;
		}
	}
	
} //SokobanCell
