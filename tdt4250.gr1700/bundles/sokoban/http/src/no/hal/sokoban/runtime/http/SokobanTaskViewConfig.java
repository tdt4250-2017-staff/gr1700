package no.hal.sokoban.runtime.http;


import org.eclipse.emf.ecore.EClass;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

import no.hal.pg.http.AppConfig;
import no.hal.sokoban.runtime.RuntimePackage;

@Component(
		service={SokobanTaskViewConfig.class, AppConfig.class},
		property={
			HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PREFIX + ":String=/web",
			HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PATTERN + ":String=/sokoban-task/*"
		}
	)
public class SokobanTaskViewConfig implements AppConfig {

	@Override
	public String getMainLocation() {
		return "/sokoban-task/SokobanTaskView.html";
	}

	@Override
	public EClass getEClass() {
		return RuntimePackage.eINSTANCE.getSokobanTaskView();
	}
}
