/**
 */
package no.hal.sokoban.model.tests;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.junit.Assert;
import org.junit.Test;

import no.hal.sokoban.model.SokobanGrid;
import no.hal.sokoban.model.SokobanLevel;
import no.hal.sokoban.model.UoD;
import no.hal.sokoban.model.util.ModelResourceFactoryImpl;

public class ModelResourceTest {

	private Collection<SokobanLevel> testLoadResource(String name, int levelCount, String levelNameFormat) {
		ResourceSet resSet = new ResourceSetImpl();
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("sok", new ModelResourceFactoryImpl());
		Resource resource = resSet.createResource(URI.createURI(name));
		try {
			resource.load(getClass().getResourceAsStream(name), null);
			Assert.assertEquals(1, resource.getContents().size());
			EObject root = resource.getContents().get(0);
			Collection<SokobanLevel> levels = null;
			if (root instanceof UoD) {
				levels = ((UoD) root).getLevels();
			} else if (root instanceof SokobanLevel) {
				levels = Collections.singletonList((SokobanLevel) root);
			} else {
				Assert.fail();
			}
			Assert.assertEquals(levelCount, levels.size());
			int count = 0;
			for (SokobanLevel level : levels) {
				String levelName = (levelNameFormat != null ? String.format(levelNameFormat, (count + 1)) : null);
				if (levelName != null) {
					Assert.assertEquals(levelName, level.getProperties().get("name"));
				}
				count++;
			}
			return levels;
		} catch (IOException e) {
			System.err.println(resource.getErrors());
		}
		return null;
	}
	
	private void testCreateGrid(SokobanLevel level) {
		SokobanGrid grid = level.createGrid();
		Assert.assertTrue(grid.getPlayerX() >= 0);
		Assert.assertTrue(grid.getPlayerY() >= 0);
	}
	private void testCreateGrid(Iterable<SokobanLevel> levels) {
		for (SokobanLevel level : levels) {
			testCreateGrid(level);
		}
	}
	
	@Test
	public void testSample1() {
		Collection<SokobanLevel> levels = testLoadResource("sample1.sok", 1, null);
		testCreateGrid(levels);
	}

	@Test
	public void testPicocosmos() {
		Collection<SokobanLevel> levels = testLoadResource("picocosmos.sok", 20, "PICOKOSMOS %02d");
		testCreateGrid(levels);
	}

	@Test
	public void testMicrocosmos() {
		Collection<SokobanLevel> levels = testLoadResource("microcosmos.sok", 40, "MICROCOSMOS %02d");
		testCreateGrid(levels);
	}
}
