/**
 */
package no.hal.sokoban.runtime;

import no.hal.pg.app.TaskView;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sokoban Task View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.hal.sokoban.runtime.SokobanTaskView#getGridView <em>Grid View</em>}</li>
 * </ul>
 *
 * @see no.hal.sokoban.runtime.RuntimePackage#getSokobanTaskView()
 * @model
 * @generated
 */
public interface SokobanTaskView extends TaskView<SokobanTask> {
	/**
	 * Returns the value of the '<em><b>Grid View</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grid View</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grid View</em>' containment reference.
	 * @see #setGridView(SokobanGridView)
	 * @see no.hal.sokoban.runtime.RuntimePackage#getSokobanTaskView_GridView()
	 * @model containment="true"
	 * @generated
	 */
	SokobanGridView getGridView();

	/**
	 * Sets the value of the '{@link no.hal.sokoban.runtime.SokobanTaskView#getGridView <em>Grid View</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Grid View</em>' containment reference.
	 * @see #getGridView()
	 * @generated
	 */
	void setGridView(SokobanGridView value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	GridRectangleValues movePlayer(String direction, Boolean stringFormat);

} // SokobanTaskView
