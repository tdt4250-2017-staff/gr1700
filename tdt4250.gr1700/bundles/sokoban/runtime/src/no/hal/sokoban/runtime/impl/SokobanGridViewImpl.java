/**
 */
package no.hal.sokoban.runtime.impl;

import java.lang.reflect.InvocationTargetException;

import no.hal.pg.app.impl.View1Impl;
import no.hal.pg.runtime.Player;
import no.hal.sokoban.model.Cell;
import no.hal.sokoban.model.SokobanGrid;
import no.hal.sokoban.runtime.GridRectangleValues;
import no.hal.sokoban.runtime.RuntimeFactory;
import no.hal.sokoban.runtime.RuntimePackage;
import no.hal.sokoban.runtime.SokobanGridView;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sokoban Grid View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link no.hal.sokoban.runtime.impl.SokobanGridViewImpl#getValues <em>Values</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SokobanGridViewImpl extends View1Impl<Player, SokobanGrid> implements SokobanGridView {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SokobanGridViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.SOKOBAN_GRID_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * This is specialized for the more specific type known in this context.
	 * @generated
	 */
	@Override
	public void setModel(SokobanGrid newModel) {
		super.setModel(newModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GridRectangleValues getValues() {
		GridRectangleValues values = basicGetValues();
		return values != null && values.eIsProxy() ? (GridRectangleValues)eResolveProxy((InternalEObject)values) : values;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public GridRectangleValues basicGetValues() {
		return getGridValues(true);
	}

	static GridRectangleValues getGridValues(SokobanGrid grid, int x, int y, int width, int height, Boolean stringFormat) {
		EList<Cell> values = grid.getGridValues(x, y, width, height);
		if (width < 0) {
			width = grid.getWidth() + width + 1 - x;
		}
		if (height < 0) {
			height = grid.getHeight() + height + 1 - y;
		}
		GridRectangleValues result = RuntimeFactory.eINSTANCE.createGridRectangleValues();
		StringBuilder builder = null;
		for (Cell cell : values) {
			if (stringFormat == null) {
				result.getValues().add(cell.toString());
			} else if (stringFormat) {
				if (builder == null) {
					builder = new StringBuilder(width);
				}
				builder.append(cell.toChar());
				if (builder.length() == width) {
					result.getValues().add(builder.toString());
					builder.setLength(0);
				}
			} else {
				result.getValues().add(cell.getName());
			}
		}
		result.setValues(x, y, width, height);
		return result;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public GridRectangleValues getGridValues(int x, int y, int width, int height, Boolean stringFormat) {
		return getGridValues(getModel() , x, y, width, height, stringFormat);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated  NOT
	 */
	public GridRectangleValues getGridValues(Boolean stringFormat) {
		return getGridValues(0, 0, -1, -1, stringFormat);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.SOKOBAN_GRID_VIEW__VALUES:
				if (resolve) return getValues();
				return basicGetValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.SOKOBAN_GRID_VIEW__VALUES:
				return basicGetValues() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.SOKOBAN_GRID_VIEW___GET_GRID_VALUES__INT_INT_INT_INT_BOOLEAN:
				return getGridValues((Integer)arguments.get(0), (Integer)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (Boolean)arguments.get(4));
			case RuntimePackage.SOKOBAN_GRID_VIEW___GET_GRID_VALUES__BOOLEAN:
				return getGridValues((Boolean)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //SokobanGridViewImpl
