/**
 */
package no.hal.sokoban.runtime;

import no.hal.pg.runtime.Task;
import no.hal.sokoban.runtime.util.SokobanResult;
import no.hal.sokoban.model.SokobanGame;
import no.hal.sokoban.model.SokobanLevel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sokoban Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.hal.sokoban.runtime.SokobanTask#getSokobanGame <em>Sokoban Game</em>}</li>
 *   <li>{@link no.hal.sokoban.runtime.SokobanTask#getSokobanLevel <em>Sokoban Level</em>}</li>
 * </ul>
 *
 * @see no.hal.sokoban.runtime.RuntimePackage#getSokobanTask()
 * @model superTypes="no.hal.pg.runtime.Task&lt;no.hal.sokoban.runtime.SokobanResult&gt;"
 * @generated
 */
public interface SokobanTask extends Task<SokobanResult> {
	/**
	 * Returns the value of the '<em><b>Sokoban Game</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sokoban Game</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sokoban Game</em>' containment reference.
	 * @see #setSokobanGame(SokobanGame)
	 * @see no.hal.sokoban.runtime.RuntimePackage#getSokobanTask_SokobanGame()
	 * @model containment="true"
	 * @generated
	 */
	SokobanGame getSokobanGame();

	/**
	 * Sets the value of the '{@link no.hal.sokoban.runtime.SokobanTask#getSokobanGame <em>Sokoban Game</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sokoban Game</em>' containment reference.
	 * @see #getSokobanGame()
	 * @generated
	 */
	void setSokobanGame(SokobanGame value);

	/**
	 * Returns the value of the '<em><b>Sokoban Level</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sokoban Level</em>' containment reference.
	 * @see #setSokobanLevel(SokobanLevel)
	 * @see no.hal.sokoban.runtime.RuntimePackage#getSokobanTask_SokobanLevel()
	 * @model containment="true"
	 * @generated
	 */
	SokobanLevel getSokobanLevel();

	/**
	 * Sets the value of the '{@link no.hal.sokoban.runtime.SokobanTask#getSokobanLevel <em>Sokoban Level</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sokoban Level</em>' containment reference.
	 * @see #getSokobanLevel()
	 * @generated
	 */
	void setSokobanLevel(SokobanLevel value);

} // SokobanTask
