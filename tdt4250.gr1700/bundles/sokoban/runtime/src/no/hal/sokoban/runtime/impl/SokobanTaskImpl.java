/**
 */
package no.hal.sokoban.runtime.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import no.hal.pg.runtime.impl.TaskImpl;
import no.hal.sokoban.runtime.util.SokobanResult;
import no.hal.sokoban.model.SokobanGame;
import no.hal.sokoban.model.SokobanLevel;
import no.hal.sokoban.runtime.RuntimePackage;
import no.hal.sokoban.runtime.SokobanTask;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sokoban Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link no.hal.sokoban.runtime.impl.SokobanTaskImpl#getSokobanGame <em>Sokoban Game</em>}</li>
 *   <li>{@link no.hal.sokoban.runtime.impl.SokobanTaskImpl#getSokobanLevel <em>Sokoban Level</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SokobanTaskImpl extends TaskImpl<SokobanResult> implements SokobanTask {
	/**
	 * The cached value of the '{@link #getSokobanGame() <em>Sokoban Game</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSokobanGame()
	 * @generated
	 * @ordered
	 */
	protected SokobanGame sokobanGame;

	/**
	 * The cached value of the '{@link #getSokobanLevel() <em>Sokoban Level</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSokobanLevel()
	 * @generated
	 * @ordered
	 */
	protected SokobanLevel sokobanLevel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SokobanTaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.SOKOBAN_TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * This is specialized for the more specific type known in this context.
	 * @generated
	 */
	@Override
	public void setResult(SokobanResult newResult) {
		super.setResult(newResult);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SokobanGame getSokobanGame() {
		return sokobanGame;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSokobanGame(SokobanGame newSokobanGame, NotificationChain msgs) {
		SokobanGame oldSokobanGame = sokobanGame;
		sokobanGame = newSokobanGame;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.SOKOBAN_TASK__SOKOBAN_GAME, oldSokobanGame, newSokobanGame);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSokobanGame(SokobanGame newSokobanGame) {
		if (newSokobanGame != sokobanGame) {
			NotificationChain msgs = null;
			if (sokobanGame != null)
				msgs = ((InternalEObject)sokobanGame).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.SOKOBAN_TASK__SOKOBAN_GAME, null, msgs);
			if (newSokobanGame != null)
				msgs = ((InternalEObject)newSokobanGame).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.SOKOBAN_TASK__SOKOBAN_GAME, null, msgs);
			msgs = basicSetSokobanGame(newSokobanGame, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SOKOBAN_TASK__SOKOBAN_GAME, newSokobanGame, newSokobanGame));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SokobanLevel getSokobanLevel() {
		return sokobanLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSokobanLevel(SokobanLevel newSokobanLevel, NotificationChain msgs) {
		SokobanLevel oldSokobanLevel = sokobanLevel;
		sokobanLevel = newSokobanLevel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.SOKOBAN_TASK__SOKOBAN_LEVEL, oldSokobanLevel, newSokobanLevel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSokobanLevel(SokobanLevel newSokobanLevel) {
		if (newSokobanLevel != sokobanLevel) {
			NotificationChain msgs = null;
			if (sokobanLevel != null)
				msgs = ((InternalEObject)sokobanLevel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.SOKOBAN_TASK__SOKOBAN_LEVEL, null, msgs);
			if (newSokobanLevel != null)
				msgs = ((InternalEObject)newSokobanLevel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.SOKOBAN_TASK__SOKOBAN_LEVEL, null, msgs);
			msgs = basicSetSokobanLevel(newSokobanLevel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SOKOBAN_TASK__SOKOBAN_LEVEL, newSokobanLevel, newSokobanLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.SOKOBAN_TASK__SOKOBAN_GAME:
				return basicSetSokobanGame(null, msgs);
			case RuntimePackage.SOKOBAN_TASK__SOKOBAN_LEVEL:
				return basicSetSokobanLevel(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.SOKOBAN_TASK__SOKOBAN_GAME:
				return getSokobanGame();
			case RuntimePackage.SOKOBAN_TASK__SOKOBAN_LEVEL:
				return getSokobanLevel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.SOKOBAN_TASK__SOKOBAN_GAME:
				setSokobanGame((SokobanGame)newValue);
				return;
			case RuntimePackage.SOKOBAN_TASK__SOKOBAN_LEVEL:
				setSokobanLevel((SokobanLevel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.SOKOBAN_TASK__SOKOBAN_GAME:
				setSokobanGame((SokobanGame)null);
				return;
			case RuntimePackage.SOKOBAN_TASK__SOKOBAN_LEVEL:
				setSokobanLevel((SokobanLevel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.SOKOBAN_TASK__SOKOBAN_GAME:
				return sokobanGame != null;
			case RuntimePackage.SOKOBAN_TASK__SOKOBAN_LEVEL:
				return sokobanLevel != null;
		}
		return super.eIsSet(featureID);
	}

	//
	
	@Override
	public String getDescription() {
		return "Solve this level: " + getSokobanGame().getLevel().getLines();
	}

	@Override
	public void start() {
		SokobanGame sokobanGame = getSokobanGame();
		sokobanGame.getUndoStack().clear();
		sokobanGame.getRedoStack().clear();
		sokobanGame.setGrid(sokobanGame.getLevel().createGrid());
		super.start();
	}

} //SokobanTaskImpl
