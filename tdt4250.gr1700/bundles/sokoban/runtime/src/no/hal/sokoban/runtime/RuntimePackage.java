/**
 */
package no.hal.sokoban.runtime;

import no.hal.gridgame.model.ModelPackage;
import no.hal.pg.app.AppPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see no.hal.sokoban.runtime.RuntimeFactory
 * @model kind="package"
 * @generated
 */
public interface RuntimePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "runtime";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/no.hal.sokoban.runtime/model/sokoban-runtime.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "sokoban-runtime";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RuntimePackage eINSTANCE = no.hal.sokoban.runtime.impl.RuntimePackageImpl.init();

	/**
	 * The meta object id for the '{@link no.hal.sokoban.runtime.impl.SokobanTaskImpl <em>Sokoban Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.hal.sokoban.runtime.impl.SokobanTaskImpl
	 * @see no.hal.sokoban.runtime.impl.RuntimePackageImpl#getSokobanTask()
	 * @generated
	 */
	int SOKOBAN_TASK = 0;

	/**
	 * The feature id for the '<em><b>Game</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK__GAME = no.hal.pg.runtime.RuntimePackage.TASK__GAME;

	/**
	 * The feature id for the '<em><b>Players</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK__PLAYERS = no.hal.pg.runtime.RuntimePackage.TASK__PLAYERS;

	/**
	 * The feature id for the '<em><b>Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK__RESULT = no.hal.pg.runtime.RuntimePackage.TASK__RESULT;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK__START_TIME = no.hal.pg.runtime.RuntimePackage.TASK__START_TIME;

	/**
	 * The feature id for the '<em><b>Finish Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK__FINISH_TIME = no.hal.pg.runtime.RuntimePackage.TASK__FINISH_TIME;

	/**
	 * The feature id for the '<em><b>Start Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK__START_CONDITIONS = no.hal.pg.runtime.RuntimePackage.TASK__START_CONDITIONS;

	/**
	 * The feature id for the '<em><b>Finish Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK__FINISH_CONDITIONS = no.hal.pg.runtime.RuntimePackage.TASK__FINISH_CONDITIONS;

	/**
	 * The feature id for the '<em><b>Start Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK__START_ACTIONS = no.hal.pg.runtime.RuntimePackage.TASK__START_ACTIONS;

	/**
	 * The feature id for the '<em><b>Finish Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK__FINISH_ACTIONS = no.hal.pg.runtime.RuntimePackage.TASK__FINISH_ACTIONS;

	/**
	 * The feature id for the '<em><b>Sokoban Game</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK__SOKOBAN_GAME = no.hal.pg.runtime.RuntimePackage.TASK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sokoban Level</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK__SOKOBAN_LEVEL = no.hal.pg.runtime.RuntimePackage.TASK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sokoban Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_FEATURE_COUNT = no.hal.pg.runtime.RuntimePackage.TASK_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK___GET_DESCRIPTION = no.hal.pg.runtime.RuntimePackage.TASK___GET_DESCRIPTION;

	/**
	 * The operation id for the '<em>Can Start</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK___CAN_START = no.hal.pg.runtime.RuntimePackage.TASK___CAN_START;

	/**
	 * The operation id for the '<em>Is Started</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK___IS_STARTED = no.hal.pg.runtime.RuntimePackage.TASK___IS_STARTED;

	/**
	 * The operation id for the '<em>Is Finished</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK___IS_FINISHED = no.hal.pg.runtime.RuntimePackage.TASK___IS_FINISHED;

	/**
	 * The operation id for the '<em>Start</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK___START = no.hal.pg.runtime.RuntimePackage.TASK___START;

	/**
	 * The operation id for the '<em>Finish</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK___FINISH__OBJECT = no.hal.pg.runtime.RuntimePackage.TASK___FINISH__OBJECT;

	/**
	 * The number of operations of the '<em>Sokoban Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_OPERATION_COUNT = no.hal.pg.runtime.RuntimePackage.TASK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '<em>Sokoban Result</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.hal.sokoban.runtime.util.SokobanResult
	 * @see no.hal.sokoban.runtime.impl.RuntimePackageImpl#getSokobanResult()
	 * @generated
	 */
	int SOKOBAN_RESULT = 4;

	/**
	 * The meta object id for the '{@link no.hal.sokoban.runtime.impl.GridRectangleValuesImpl <em>Grid Rectangle Values</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.hal.sokoban.runtime.impl.GridRectangleValuesImpl
	 * @see no.hal.sokoban.runtime.impl.RuntimePackageImpl#getGridRectangleValues()
	 * @generated
	 */
	int GRID_RECTANGLE_VALUES = 1;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_RECTANGLE_VALUES__X = ModelPackage.GRID_RECTANGLE__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_RECTANGLE_VALUES__Y = ModelPackage.GRID_RECTANGLE__Y;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_RECTANGLE_VALUES__WIDTH = ModelPackage.GRID_RECTANGLE__WIDTH;

	/**
	 * The feature id for the '<em><b>Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_RECTANGLE_VALUES__HEIGHT = ModelPackage.GRID_RECTANGLE__HEIGHT;

	/**
	 * The feature id for the '<em><b>Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_RECTANGLE_VALUES__VALUES = ModelPackage.GRID_RECTANGLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Grid Rectangle Values</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_RECTANGLE_VALUES_FEATURE_COUNT = ModelPackage.GRID_RECTANGLE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Set Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_RECTANGLE_VALUES___SET_VALUES__INT_INT = ModelPackage.GRID_RECTANGLE___SET_VALUES__INT_INT;

	/**
	 * The operation id for the '<em>Set Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_RECTANGLE_VALUES___SET_VALUES__INT_INT_INT_INT = ModelPackage.GRID_RECTANGLE___SET_VALUES__INT_INT_INT_INT;

	/**
	 * The operation id for the '<em>Set Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_RECTANGLE_VALUES___SET_VALUES__GRIDRECTANGLE = ModelPackage.GRID_RECTANGLE___SET_VALUES__GRIDRECTANGLE;

	/**
	 * The number of operations of the '<em>Grid Rectangle Values</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_RECTANGLE_VALUES_OPERATION_COUNT = ModelPackage.GRID_RECTANGLE_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link no.hal.sokoban.runtime.impl.SokobanTaskViewImpl <em>Sokoban Task View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.hal.sokoban.runtime.impl.SokobanTaskViewImpl
	 * @see no.hal.sokoban.runtime.impl.RuntimePackageImpl#getSokobanTaskView()
	 * @generated
	 */
	int SOKOBAN_TASK_VIEW = 2;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_VIEW__USER = AppPackage.TASK_VIEW__USER;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_VIEW__MODEL = AppPackage.TASK_VIEW__MODEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_VIEW__DESCRIPTION = AppPackage.TASK_VIEW__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_VIEW__ENABLED = AppPackage.TASK_VIEW__ENABLED;

	/**
	 * The feature id for the '<em><b>Started</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_VIEW__STARTED = AppPackage.TASK_VIEW__STARTED;

	/**
	 * The feature id for the '<em><b>Finished</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_VIEW__FINISHED = AppPackage.TASK_VIEW__FINISHED;

	/**
	 * The feature id for the '<em><b>Grid View</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_VIEW__GRID_VIEW = AppPackage.TASK_VIEW_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sokoban Task View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_VIEW_FEATURE_COUNT = AppPackage.TASK_VIEW_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Start</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_VIEW___START = AppPackage.TASK_VIEW___START;

	/**
	 * The operation id for the '<em>Move Player</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_VIEW___MOVE_PLAYER__STRING_BOOLEAN = AppPackage.TASK_VIEW_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Sokoban Task View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_TASK_VIEW_OPERATION_COUNT = AppPackage.TASK_VIEW_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link no.hal.sokoban.runtime.impl.SokobanGridViewImpl <em>Sokoban Grid View</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.hal.sokoban.runtime.impl.SokobanGridViewImpl
	 * @see no.hal.sokoban.runtime.impl.RuntimePackageImpl#getSokobanGridView()
	 * @generated
	 */
	int SOKOBAN_GRID_VIEW = 3;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_GRID_VIEW__USER = AppPackage.VIEW1__USER;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_GRID_VIEW__MODEL = AppPackage.VIEW1__MODEL;

	/**
	 * The feature id for the '<em><b>Values</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_GRID_VIEW__VALUES = AppPackage.VIEW1_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sokoban Grid View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_GRID_VIEW_FEATURE_COUNT = AppPackage.VIEW1_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Grid Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_GRID_VIEW___GET_GRID_VALUES__INT_INT_INT_INT_BOOLEAN = AppPackage.VIEW1_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Grid Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_GRID_VIEW___GET_GRID_VALUES__BOOLEAN = AppPackage.VIEW1_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Sokoban Grid View</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOKOBAN_GRID_VIEW_OPERATION_COUNT = AppPackage.VIEW1_OPERATION_COUNT + 2;

	/**
	 * Returns the meta object for class '{@link no.hal.sokoban.runtime.SokobanTask <em>Sokoban Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sokoban Task</em>'.
	 * @see no.hal.sokoban.runtime.SokobanTask
	 * @generated
	 */
	EClass getSokobanTask();

	/**
	 * Returns the meta object for the containment reference '{@link no.hal.sokoban.runtime.SokobanTask#getSokobanGame <em>Sokoban Game</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sokoban Game</em>'.
	 * @see no.hal.sokoban.runtime.SokobanTask#getSokobanGame()
	 * @see #getSokobanTask()
	 * @generated
	 */
	EReference getSokobanTask_SokobanGame();

	/**
	 * Returns the meta object for the containment reference '{@link no.hal.sokoban.runtime.SokobanTask#getSokobanLevel <em>Sokoban Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sokoban Level</em>'.
	 * @see no.hal.sokoban.runtime.SokobanTask#getSokobanLevel()
	 * @see #getSokobanTask()
	 * @generated
	 */
	EReference getSokobanTask_SokobanLevel();

	/**
	 * Returns the meta object for data type '{@link no.hal.sokoban.runtime.util.SokobanResult <em>Sokoban Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Sokoban Result</em>'.
	 * @see no.hal.sokoban.runtime.util.SokobanResult
	 * @model instanceClass="no.hal.sokoban.runtime.util.SokobanResult"
	 * @generated
	 */
	EDataType getSokobanResult();

	/**
	 * Returns the meta object for class '{@link no.hal.sokoban.runtime.GridRectangleValues <em>Grid Rectangle Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Grid Rectangle Values</em>'.
	 * @see no.hal.sokoban.runtime.GridRectangleValues
	 * @generated
	 */
	EClass getGridRectangleValues();

	/**
	 * Returns the meta object for the attribute list '{@link no.hal.sokoban.runtime.GridRectangleValues#getValues <em>Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Values</em>'.
	 * @see no.hal.sokoban.runtime.GridRectangleValues#getValues()
	 * @see #getGridRectangleValues()
	 * @generated
	 */
	EAttribute getGridRectangleValues_Values();

	/**
	 * Returns the meta object for class '{@link no.hal.sokoban.runtime.SokobanTaskView <em>Sokoban Task View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sokoban Task View</em>'.
	 * @see no.hal.sokoban.runtime.SokobanTaskView
	 * @generated
	 */
	EClass getSokobanTaskView();

	/**
	 * Returns the meta object for the containment reference '{@link no.hal.sokoban.runtime.SokobanTaskView#getGridView <em>Grid View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Grid View</em>'.
	 * @see no.hal.sokoban.runtime.SokobanTaskView#getGridView()
	 * @see #getSokobanTaskView()
	 * @generated
	 */
	EReference getSokobanTaskView_GridView();

	/**
	 * Returns the meta object for the '{@link no.hal.sokoban.runtime.SokobanTaskView#movePlayer(java.lang.String, java.lang.Boolean) <em>Move Player</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Player</em>' operation.
	 * @see no.hal.sokoban.runtime.SokobanTaskView#movePlayer(java.lang.String, java.lang.Boolean)
	 * @generated
	 */
	EOperation getSokobanTaskView__MovePlayer__String_Boolean();

	/**
	 * Returns the meta object for class '{@link no.hal.sokoban.runtime.SokobanGridView <em>Sokoban Grid View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sokoban Grid View</em>'.
	 * @see no.hal.sokoban.runtime.SokobanGridView
	 * @generated
	 */
	EClass getSokobanGridView();

	/**
	 * Returns the meta object for the reference '{@link no.hal.sokoban.runtime.SokobanGridView#getValues <em>Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Values</em>'.
	 * @see no.hal.sokoban.runtime.SokobanGridView#getValues()
	 * @see #getSokobanGridView()
	 * @generated
	 */
	EReference getSokobanGridView_Values();

	/**
	 * Returns the meta object for the '{@link no.hal.sokoban.runtime.SokobanGridView#getGridValues(int, int, int, int, java.lang.Boolean) <em>Get Grid Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Grid Values</em>' operation.
	 * @see no.hal.sokoban.runtime.SokobanGridView#getGridValues(int, int, int, int, java.lang.Boolean)
	 * @generated
	 */
	EOperation getSokobanGridView__GetGridValues__int_int_int_int_Boolean();

	/**
	 * Returns the meta object for the '{@link no.hal.sokoban.runtime.SokobanGridView#getGridValues(java.lang.Boolean) <em>Get Grid Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Grid Values</em>' operation.
	 * @see no.hal.sokoban.runtime.SokobanGridView#getGridValues(java.lang.Boolean)
	 * @generated
	 */
	EOperation getSokobanGridView__GetGridValues__Boolean();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RuntimeFactory getRuntimeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link no.hal.sokoban.runtime.impl.SokobanTaskImpl <em>Sokoban Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.hal.sokoban.runtime.impl.SokobanTaskImpl
		 * @see no.hal.sokoban.runtime.impl.RuntimePackageImpl#getSokobanTask()
		 * @generated
		 */
		EClass SOKOBAN_TASK = eINSTANCE.getSokobanTask();

		/**
		 * The meta object literal for the '<em><b>Sokoban Game</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOKOBAN_TASK__SOKOBAN_GAME = eINSTANCE.getSokobanTask_SokobanGame();

		/**
		 * The meta object literal for the '<em><b>Sokoban Level</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOKOBAN_TASK__SOKOBAN_LEVEL = eINSTANCE.getSokobanTask_SokobanLevel();

		/**
		 * The meta object literal for the '<em>Sokoban Result</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.hal.sokoban.runtime.util.SokobanResult
		 * @see no.hal.sokoban.runtime.impl.RuntimePackageImpl#getSokobanResult()
		 * @generated
		 */
		EDataType SOKOBAN_RESULT = eINSTANCE.getSokobanResult();

		/**
		 * The meta object literal for the '{@link no.hal.sokoban.runtime.impl.GridRectangleValuesImpl <em>Grid Rectangle Values</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.hal.sokoban.runtime.impl.GridRectangleValuesImpl
		 * @see no.hal.sokoban.runtime.impl.RuntimePackageImpl#getGridRectangleValues()
		 * @generated
		 */
		EClass GRID_RECTANGLE_VALUES = eINSTANCE.getGridRectangleValues();

		/**
		 * The meta object literal for the '<em><b>Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRID_RECTANGLE_VALUES__VALUES = eINSTANCE.getGridRectangleValues_Values();

		/**
		 * The meta object literal for the '{@link no.hal.sokoban.runtime.impl.SokobanTaskViewImpl <em>Sokoban Task View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.hal.sokoban.runtime.impl.SokobanTaskViewImpl
		 * @see no.hal.sokoban.runtime.impl.RuntimePackageImpl#getSokobanTaskView()
		 * @generated
		 */
		EClass SOKOBAN_TASK_VIEW = eINSTANCE.getSokobanTaskView();

		/**
		 * The meta object literal for the '<em><b>Grid View</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOKOBAN_TASK_VIEW__GRID_VIEW = eINSTANCE.getSokobanTaskView_GridView();

		/**
		 * The meta object literal for the '<em><b>Move Player</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOKOBAN_TASK_VIEW___MOVE_PLAYER__STRING_BOOLEAN = eINSTANCE.getSokobanTaskView__MovePlayer__String_Boolean();

		/**
		 * The meta object literal for the '{@link no.hal.sokoban.runtime.impl.SokobanGridViewImpl <em>Sokoban Grid View</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.hal.sokoban.runtime.impl.SokobanGridViewImpl
		 * @see no.hal.sokoban.runtime.impl.RuntimePackageImpl#getSokobanGridView()
		 * @generated
		 */
		EClass SOKOBAN_GRID_VIEW = eINSTANCE.getSokobanGridView();

		/**
		 * The meta object literal for the '<em><b>Values</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOKOBAN_GRID_VIEW__VALUES = eINSTANCE.getSokobanGridView_Values();

		/**
		 * The meta object literal for the '<em><b>Get Grid Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOKOBAN_GRID_VIEW___GET_GRID_VALUES__INT_INT_INT_INT_BOOLEAN = eINSTANCE.getSokobanGridView__GetGridValues__int_int_int_int_Boolean();

		/**
		 * The meta object literal for the '<em><b>Get Grid Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SOKOBAN_GRID_VIEW___GET_GRID_VALUES__BOOLEAN = eINSTANCE.getSokobanGridView__GetGridValues__Boolean();

	}

} //RuntimePackage
