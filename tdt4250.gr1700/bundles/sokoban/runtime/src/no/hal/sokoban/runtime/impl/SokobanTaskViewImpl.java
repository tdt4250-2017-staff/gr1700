/**
 */
package no.hal.sokoban.runtime.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import no.hal.gridgame.model.GridChangeDescription;
import no.hal.pg.app.impl.TaskViewImpl;
import no.hal.sokoban.model.Direction;
import no.hal.sokoban.model.ModelFactory;
import no.hal.sokoban.model.MovePlayerCommand;
import no.hal.sokoban.model.SokobanGame;
import no.hal.sokoban.model.SokobanGrid;
import no.hal.sokoban.runtime.GridRectangleValues;
import no.hal.sokoban.runtime.RuntimeFactory;
import no.hal.sokoban.runtime.RuntimePackage;
import no.hal.sokoban.runtime.SokobanGridView;
import no.hal.sokoban.runtime.SokobanTask;
import no.hal.sokoban.runtime.SokobanTaskView;
import no.hal.sokoban.runtime.util.SokobanResult;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sokoban Task View</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link no.hal.sokoban.runtime.impl.SokobanTaskViewImpl#getGridView <em>Grid View</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SokobanTaskViewImpl extends TaskViewImpl<SokobanTask> implements SokobanTaskView {
	/**
	 * The cached value of the '{@link #getGridView() <em>Grid View</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGridView()
	 * @generated
	 * @ordered
	 */
	protected SokobanGridView gridView;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SokobanTaskViewImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RuntimePackage.Literals.SOKOBAN_TASK_VIEW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public SokobanGridView getGridView() {
		if (gridView == null) {
			SokobanGridView gridView = RuntimeFactory.eINSTANCE.createSokobanGridView();
			this.setGridView(gridView);
		}
		SokobanGrid grid = getModel().getSokobanGame().getGrid();
		if (gridView.getModel() != grid) {
			gridView.setModel(grid);
		}
		return gridView;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGridView(SokobanGridView newGridView, NotificationChain msgs) {
		SokobanGridView oldGridView = gridView;
		gridView = newGridView;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RuntimePackage.SOKOBAN_TASK_VIEW__GRID_VIEW, oldGridView, newGridView);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGridView(SokobanGridView newGridView) {
		if (newGridView != gridView) {
			NotificationChain msgs = null;
			if (gridView != null)
				msgs = ((InternalEObject)gridView).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.SOKOBAN_TASK_VIEW__GRID_VIEW, null, msgs);
			if (newGridView != null)
				msgs = ((InternalEObject)newGridView).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RuntimePackage.SOKOBAN_TASK_VIEW__GRID_VIEW, null, msgs);
			msgs = basicSetGridView(newGridView, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RuntimePackage.SOKOBAN_TASK_VIEW__GRID_VIEW, newGridView, newGridView));
	}

	public GridChangeDescription movePlayer(String direction) {
		SokobanGame game = getModel().getSokobanGame();
		Collection<MovePlayerCommand> commands = new ArrayList<MovePlayerCommand>();
		for (int i = 0; i < direction.length(); i++) {
			MovePlayerCommand command = ModelFactory.eINSTANCE.createMovePlayerCommand();
			command.setDirection(Direction.valueOf(direction.charAt(i)));
			command.setGrid(game.getGrid());
			game.perform(command);
			commands.add(command);
			if (game.isFinished()) {
				EList<MovePlayerCommand> allCommands = game.getUndoStack();
				StringBuilder builder = new StringBuilder(allCommands.size());
				for (MovePlayerCommand moveCommand : allCommands) {
					String s = moveCommand.getDirection().getLiteral();
					builder.append(moveCommand.isPush() ? s.toUpperCase() : s);
				}
				SokobanResult result = SokobanResult.valueOf(builder.toString());
				getModel().finish(result);
				break;
			}
		}
		switch (commands.size()) {
			case 0: return null;
			case 1: return EcoreUtil.copy(commands.iterator().next().getChanges());
			default: {
				GridChangeDescription totalChange = no.hal.gridgame.model.ModelFactory.eINSTANCE.createGridChangeDescription();
				for (MovePlayerCommand command : commands) {
					totalChange.addChange(command.getChanges());
				}
				return totalChange;
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public GridRectangleValues movePlayer(String direction, Boolean stringFormat) {
		GridChangeDescription changes = null;
		if (isActive()) {
			changes = movePlayer(direction);
		}
		if (changes != null) {
			return SokobanGridViewImpl.getGridValues(getGridView().getModel(), changes.getX(), changes.getY(), changes.getWidth(), changes.getHeight(), stringFormat);
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RuntimePackage.SOKOBAN_TASK_VIEW__GRID_VIEW:
				return basicSetGridView(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RuntimePackage.SOKOBAN_TASK_VIEW__GRID_VIEW:
				return getGridView();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RuntimePackage.SOKOBAN_TASK_VIEW__GRID_VIEW:
				setGridView((SokobanGridView)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RuntimePackage.SOKOBAN_TASK_VIEW__GRID_VIEW:
				setGridView((SokobanGridView)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RuntimePackage.SOKOBAN_TASK_VIEW__GRID_VIEW:
				return gridView != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RuntimePackage.SOKOBAN_TASK_VIEW___MOVE_PLAYER__STRING_BOOLEAN:
				return movePlayer((String)arguments.get(0), (Boolean)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}
	
} //SokobanTaskViewImpl
