/**
 */
package no.hal.sokoban.runtime.impl;

import no.hal.gridgame.model.ModelPackage;
import no.hal.pg.app.AppPackage;
import no.hal.pg.arc.ArcPackage;
import no.hal.pg.osm.OsmPackage;
import no.hal.sokoban.runtime.util.SokobanResult;
import no.hal.sokoban.runtime.GridRectangleValues;
import no.hal.sokoban.runtime.RuntimeFactory;
import no.hal.sokoban.runtime.RuntimePackage;
import no.hal.sokoban.runtime.SokobanGridView;
import no.hal.sokoban.runtime.SokobanTask;
import no.hal.sokoban.runtime.SokobanTaskView;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RuntimePackageImpl extends EPackageImpl implements RuntimePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sokobanTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gridRectangleValuesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sokobanTaskViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sokobanGridViewEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType sokobanResultEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see no.hal.sokoban.runtime.RuntimePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RuntimePackageImpl() {
		super(eNS_URI, RuntimeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link RuntimePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RuntimePackage init() {
		if (isInited) return (RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(RuntimePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredRuntimePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		RuntimePackageImpl theRuntimePackage = registeredRuntimePackage instanceof RuntimePackageImpl ? (RuntimePackageImpl)registeredRuntimePackage : new RuntimePackageImpl();

		isInited = true;

		// Initialize simple dependencies
		AppPackage.eINSTANCE.eClass();
		ArcPackage.eINSTANCE.eClass();
		ModelPackage.eINSTANCE.eClass();
		OsmPackage.eINSTANCE.eClass();
		no.hal.pg.runtime.RuntimePackage.eINSTANCE.eClass();
		no.hal.sokoban.model.ModelPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theRuntimePackage.createPackageContents();

		// Initialize created meta-data
		theRuntimePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRuntimePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RuntimePackage.eNS_URI, theRuntimePackage);
		return theRuntimePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSokobanTask() {
		return sokobanTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSokobanTask_SokobanGame() {
		return (EReference)sokobanTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSokobanTask_SokobanLevel() {
		return (EReference)sokobanTaskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getSokobanResult() {
		return sokobanResultEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGridRectangleValues() {
		return gridRectangleValuesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGridRectangleValues_Values() {
		return (EAttribute)gridRectangleValuesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSokobanTaskView() {
		return sokobanTaskViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSokobanTaskView_GridView() {
		return (EReference)sokobanTaskViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSokobanTaskView__MovePlayer__String_Boolean() {
		return sokobanTaskViewEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSokobanGridView() {
		return sokobanGridViewEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSokobanGridView_Values() {
		return (EReference)sokobanGridViewEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSokobanGridView__GetGridValues__int_int_int_int_Boolean() {
		return sokobanGridViewEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSokobanGridView__GetGridValues__Boolean() {
		return sokobanGridViewEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RuntimeFactory getRuntimeFactory() {
		return (RuntimeFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		sokobanTaskEClass = createEClass(SOKOBAN_TASK);
		createEReference(sokobanTaskEClass, SOKOBAN_TASK__SOKOBAN_GAME);
		createEReference(sokobanTaskEClass, SOKOBAN_TASK__SOKOBAN_LEVEL);

		gridRectangleValuesEClass = createEClass(GRID_RECTANGLE_VALUES);
		createEAttribute(gridRectangleValuesEClass, GRID_RECTANGLE_VALUES__VALUES);

		sokobanTaskViewEClass = createEClass(SOKOBAN_TASK_VIEW);
		createEReference(sokobanTaskViewEClass, SOKOBAN_TASK_VIEW__GRID_VIEW);
		createEOperation(sokobanTaskViewEClass, SOKOBAN_TASK_VIEW___MOVE_PLAYER__STRING_BOOLEAN);

		sokobanGridViewEClass = createEClass(SOKOBAN_GRID_VIEW);
		createEReference(sokobanGridViewEClass, SOKOBAN_GRID_VIEW__VALUES);
		createEOperation(sokobanGridViewEClass, SOKOBAN_GRID_VIEW___GET_GRID_VALUES__INT_INT_INT_INT_BOOLEAN);
		createEOperation(sokobanGridViewEClass, SOKOBAN_GRID_VIEW___GET_GRID_VALUES__BOOLEAN);

		// Create data types
		sokobanResultEDataType = createEDataType(SOKOBAN_RESULT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		no.hal.pg.runtime.RuntimePackage theRuntimePackage_1 = (no.hal.pg.runtime.RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(no.hal.pg.runtime.RuntimePackage.eNS_URI);
		no.hal.sokoban.model.ModelPackage theModelPackage_1 = (no.hal.sokoban.model.ModelPackage)EPackage.Registry.INSTANCE.getEPackage(no.hal.sokoban.model.ModelPackage.eNS_URI);
		ModelPackage theModelPackage = (ModelPackage)EPackage.Registry.INSTANCE.getEPackage(ModelPackage.eNS_URI);
		AppPackage theAppPackage = (AppPackage)EPackage.Registry.INSTANCE.getEPackage(AppPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		EGenericType g1 = createEGenericType(theRuntimePackage_1.getTask());
		EGenericType g2 = createEGenericType(this.getSokobanResult());
		g1.getETypeArguments().add(g2);
		sokobanTaskEClass.getEGenericSuperTypes().add(g1);
		gridRectangleValuesEClass.getESuperTypes().add(theModelPackage.getGridRectangle());
		g1 = createEGenericType(theAppPackage.getTaskView());
		g2 = createEGenericType(this.getSokobanTask());
		g1.getETypeArguments().add(g2);
		sokobanTaskViewEClass.getEGenericSuperTypes().add(g1);
		g1 = createEGenericType(theAppPackage.getView1());
		g2 = createEGenericType(theRuntimePackage_1.getPlayer());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theModelPackage_1.getSokobanGrid());
		g1.getETypeArguments().add(g2);
		sokobanGridViewEClass.getEGenericSuperTypes().add(g1);

		// Initialize classes, features, and operations; add parameters
		initEClass(sokobanTaskEClass, SokobanTask.class, "SokobanTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSokobanTask_SokobanGame(), theModelPackage_1.getSokobanGame(), null, "sokobanGame", null, 0, 1, SokobanTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSokobanTask_SokobanLevel(), theModelPackage_1.getSokobanLevel(), null, "sokobanLevel", null, 0, 1, SokobanTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(gridRectangleValuesEClass, GridRectangleValues.class, "GridRectangleValues", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGridRectangleValues_Values(), ecorePackage.getEString(), "values", null, 0, -1, GridRectangleValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sokobanTaskViewEClass, SokobanTaskView.class, "SokobanTaskView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSokobanTaskView_GridView(), this.getSokobanGridView(), null, "gridView", null, 0, 1, SokobanTaskView.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getSokobanTaskView__MovePlayer__String_Boolean(), this.getGridRectangleValues(), "movePlayer", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "direction", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBooleanObject(), "stringFormat", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(sokobanGridViewEClass, SokobanGridView.class, "SokobanGridView", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSokobanGridView_Values(), this.getGridRectangleValues(), null, "values", null, 0, 1, SokobanGridView.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = initEOperation(getSokobanGridView__GetGridValues__int_int_int_int_Boolean(), this.getGridRectangleValues(), "getGridValues", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "x", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "y", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "width", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "height", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBooleanObject(), "stringFormat", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getSokobanGridView__GetGridValues__Boolean(), this.getGridRectangleValues(), "getGridValues", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBooleanObject(), "stringFormat", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize data types
		initEDataType(sokobanResultEDataType, SokobanResult.class, "SokobanResult", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //RuntimePackageImpl
