package no.hal.sokoban.runtime.util;

import org.eclipse.emf.ecore.EObject;
import org.osgi.service.component.annotations.Component;

import no.hal.pg.app.View;

import no.hal.pg.app.util.IViewFactory;
import no.hal.sokoban.runtime.RuntimeFactory;
import no.hal.sokoban.runtime.SokobanTask;

@Component
public class SokobanTaskViewFactory implements IViewFactory {

	@Override
	public View<?> createView(EObject user, EObject eObject) {
		if (eObject instanceof SokobanTask) {
			return RuntimeFactory.eINSTANCE.createSokobanTaskView();
		}
		return null;
	}
}
