/**
 */
package no.hal.sokoban.runtime;

import no.hal.pg.app.View1;
import no.hal.pg.runtime.Player;
import no.hal.sokoban.model.SokobanGrid;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sokoban Grid View</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.hal.sokoban.runtime.SokobanGridView#getValues <em>Values</em>}</li>
 * </ul>
 *
 * @see no.hal.sokoban.runtime.RuntimePackage#getSokobanGridView()
 * @model
 * @generated
 */
public interface SokobanGridView extends View1<Player, SokobanGrid> {
	/**
	 * Returns the value of the '<em><b>Values</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Values</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' reference.
	 * @see no.hal.sokoban.runtime.RuntimePackage#getSokobanGridView_Values()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	GridRectangleValues getValues();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	GridRectangleValues getGridValues(int x, int y, int width, int height, Boolean stringFormat);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	GridRectangleValues getGridValues(Boolean stringFormat);

} // SokobanGridView
