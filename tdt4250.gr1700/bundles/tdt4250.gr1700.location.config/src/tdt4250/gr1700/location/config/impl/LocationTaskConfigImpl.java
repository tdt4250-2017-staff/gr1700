/**
 */
package tdt4250.gr1700.location.config.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import no.hal.pg.config.TaskProxy;
import no.hal.pg.config.impl.TaskConfigImpl;
import no.hal.pg.osm.GeoLocated;
import no.hal.pg.osm.OsmPackage;
import no.hal.pg.osm.geoutil.LatLong;
import tdt4250.gr1700.location.DistanceHint;
import tdt4250.gr1700.location.LocationFactory;
import tdt4250.gr1700.location.LocationTask;
import tdt4250.gr1700.location.config.ConfigPackage;
import tdt4250.gr1700.location.config.LocationTaskConfig;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Location Task Config</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.gr1700.location.config.impl.LocationTaskConfigImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.config.impl.LocationTaskConfigImpl#getTargetDistance <em>Target Distance</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.config.impl.LocationTaskConfigImpl#getHints <em>Hints</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LocationTaskConfigImpl extends TaskConfigImpl<LocationTask> implements LocationTaskConfig {
	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected GeoLocated location;

	/**
	 * The default value of the '{@link #getTargetDistance() <em>Target Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetDistance()
	 * @generated
	 * @ordered
	 */
	protected static final int TARGET_DISTANCE_EDEFAULT = 10;

	/**
	 * The cached value of the '{@link #getTargetDistance() <em>Target Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetDistance()
	 * @generated
	 * @ordered
	 */
	protected int targetDistance = TARGET_DISTANCE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHints() <em>Hints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHints()
	 * @generated
	 * @ordered
	 */
	protected EList<DistanceHint> hints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocationTaskConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigPackage.Literals.LOCATION_TASK_CONFIG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeoLocated getLocation() {
		if (location != null && location.eIsProxy()) {
			InternalEObject oldLocation = (InternalEObject)location;
			location = (GeoLocated)eResolveProxy(oldLocation);
			if (location != oldLocation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConfigPackage.LOCATION_TASK_CONFIG__LOCATION, oldLocation, location));
			}
		}
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeoLocated basicGetLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(GeoLocated newLocation) {
		GeoLocated oldLocation = location;
		location = newLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.LOCATION_TASK_CONFIG__LOCATION, oldLocation, location));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTargetDistance() {
		return targetDistance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetDistance(int newTargetDistance) {
		int oldTargetDistance = targetDistance;
		targetDistance = newTargetDistance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigPackage.LOCATION_TASK_CONFIG__TARGET_DISTANCE, oldTargetDistance, targetDistance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DistanceHint> getHints() {
		if (hints == null) {
			hints = new EObjectContainmentEList<DistanceHint>(DistanceHint.class, this, ConfigPackage.LOCATION_TASK_CONFIG__HINTS);
		}
		return hints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public LatLong getLatLong() {
		return (getLocation() != null ? getLocation().getLatLong() : null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConfigPackage.LOCATION_TASK_CONFIG__HINTS:
				return ((InternalEList<?>)getHints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigPackage.LOCATION_TASK_CONFIG__LOCATION:
				if (resolve) return getLocation();
				return basicGetLocation();
			case ConfigPackage.LOCATION_TASK_CONFIG__TARGET_DISTANCE:
				return getTargetDistance();
			case ConfigPackage.LOCATION_TASK_CONFIG__HINTS:
				return getHints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigPackage.LOCATION_TASK_CONFIG__LOCATION:
				setLocation((GeoLocated)newValue);
				return;
			case ConfigPackage.LOCATION_TASK_CONFIG__TARGET_DISTANCE:
				setTargetDistance((Integer)newValue);
				return;
			case ConfigPackage.LOCATION_TASK_CONFIG__HINTS:
				getHints().clear();
				getHints().addAll((Collection<? extends DistanceHint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigPackage.LOCATION_TASK_CONFIG__LOCATION:
				setLocation((GeoLocated)null);
				return;
			case ConfigPackage.LOCATION_TASK_CONFIG__TARGET_DISTANCE:
				setTargetDistance(TARGET_DISTANCE_EDEFAULT);
				return;
			case ConfigPackage.LOCATION_TASK_CONFIG__HINTS:
				getHints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigPackage.LOCATION_TASK_CONFIG__LOCATION:
				return location != null;
			case ConfigPackage.LOCATION_TASK_CONFIG__TARGET_DISTANCE:
				return targetDistance != TARGET_DISTANCE_EDEFAULT;
			case ConfigPackage.LOCATION_TASK_CONFIG__HINTS:
				return hints != null && !hints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == GeoLocated.class) {
			switch (baseOperationID) {
				case OsmPackage.GEO_LOCATED___GET_LAT_LONG: return ConfigPackage.LOCATION_TASK_CONFIG___GET_LAT_LONG;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConfigPackage.LOCATION_TASK_CONFIG___GET_LAT_LONG:
				return getLatLong();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (targetDistance: ");
		result.append(targetDistance);
		result.append(')');
		return result.toString();
	}
	
	//

	@Override
	public LocationTask createTask(TaskProxy proxy) {
		LocationTask locationTask = LocationFactory.eINSTANCE.createLocationTask();
		LatLong latLong = getLocation().getLatLong();
		locationTask.setLatitude((float) latLong.latitude);
		locationTask.setLongitude((float) latLong.longitude);
		locationTask.setTargetDistance(getTargetDistance());
		locationTask.getHints().addAll(EcoreUtil.copyAll(getHints()));
		return locationTask;
	}

} //LocationTaskConfigImpl
