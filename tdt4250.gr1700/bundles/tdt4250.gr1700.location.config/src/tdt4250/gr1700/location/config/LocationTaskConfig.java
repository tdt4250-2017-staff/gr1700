/**
 */
package tdt4250.gr1700.location.config;

import no.hal.pg.config.TaskConfig;

import no.hal.pg.osm.GeoLocated;

import org.eclipse.emf.common.util.EList;

import tdt4250.gr1700.location.DistanceHint;
import tdt4250.gr1700.location.LocationTask;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Location Task Config</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.gr1700.location.config.LocationTaskConfig#getLocation <em>Location</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.config.LocationTaskConfig#getTargetDistance <em>Target Distance</em>}</li>
 *   <li>{@link tdt4250.gr1700.location.config.LocationTaskConfig#getHints <em>Hints</em>}</li>
 * </ul>
 *
 * @see tdt4250.gr1700.location.config.ConfigPackage#getLocationTaskConfig()
 * @model
 * @generated
 */
public interface LocationTaskConfig extends TaskConfig<LocationTask>, GeoLocated {
	/**
	 * Returns the value of the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' reference.
	 * @see #setLocation(GeoLocated)
	 * @see tdt4250.gr1700.location.config.ConfigPackage#getLocationTaskConfig_Location()
	 * @model
	 * @generated
	 */
	GeoLocated getLocation();

	/**
	 * Sets the value of the '{@link tdt4250.gr1700.location.config.LocationTaskConfig#getLocation <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' reference.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(GeoLocated value);

	/**
	 * Returns the value of the '<em><b>Target Distance</b></em>' attribute.
	 * The default value is <code>"10"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Distance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Distance</em>' attribute.
	 * @see #setTargetDistance(int)
	 * @see tdt4250.gr1700.location.config.ConfigPackage#getLocationTaskConfig_TargetDistance()
	 * @model default="10"
	 * @generated
	 */
	int getTargetDistance();

	/**
	 * Sets the value of the '{@link tdt4250.gr1700.location.config.LocationTaskConfig#getTargetDistance <em>Target Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Distance</em>' attribute.
	 * @see #getTargetDistance()
	 * @generated
	 */
	void setTargetDistance(int value);

	/**
	 * Returns the value of the '<em><b>Hints</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.gr1700.location.DistanceHint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hints</em>' containment reference list.
	 * @see tdt4250.gr1700.location.config.ConfigPackage#getLocationTaskConfig_Hints()
	 * @model containment="true"
	 * @generated
	 */
	EList<DistanceHint> getHints();

} // LocationTaskConfig
