/**
 */
package tdt4250.gr1700.location.config;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see tdt4250.gr1700.location.config.ConfigFactory
 * @model kind="package"
 * @generated
 */
public interface ConfigPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "config";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/tdt4250.gr1700.location.config/model/location-config.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "location-config";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConfigPackage eINSTANCE = tdt4250.gr1700.location.config.impl.ConfigPackageImpl.init();

	/**
	 * The meta object id for the '{@link tdt4250.gr1700.location.config.impl.LocationTaskConfigImpl <em>Location Task Config</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.gr1700.location.config.impl.LocationTaskConfigImpl
	 * @see tdt4250.gr1700.location.config.impl.ConfigPackageImpl#getLocationTaskConfig()
	 * @generated
	 */
	int LOCATION_TASK_CONFIG = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_CONFIG__NAME = no.hal.pg.config.ConfigPackage.TASK_CONFIG__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_CONFIG__DESCRIPTION = no.hal.pg.config.ConfigPackage.TASK_CONFIG__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_CONFIG__LOCATION = no.hal.pg.config.ConfigPackage.TASK_CONFIG_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target Distance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_CONFIG__TARGET_DISTANCE = no.hal.pg.config.ConfigPackage.TASK_CONFIG_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Hints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_CONFIG__HINTS = no.hal.pg.config.ConfigPackage.TASK_CONFIG_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Location Task Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_CONFIG_FEATURE_COUNT = no.hal.pg.config.ConfigPackage.TASK_CONFIG_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Create Task</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_CONFIG___CREATE_TASK__TASKPROXY = no.hal.pg.config.ConfigPackage.TASK_CONFIG___CREATE_TASK__TASKPROXY;

	/**
	 * The operation id for the '<em>Get Lat Long</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_CONFIG___GET_LAT_LONG = no.hal.pg.config.ConfigPackage.TASK_CONFIG_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Location Task Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_TASK_CONFIG_OPERATION_COUNT = no.hal.pg.config.ConfigPackage.TASK_CONFIG_OPERATION_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link tdt4250.gr1700.location.config.LocationTaskConfig <em>Location Task Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Location Task Config</em>'.
	 * @see tdt4250.gr1700.location.config.LocationTaskConfig
	 * @generated
	 */
	EClass getLocationTaskConfig();

	/**
	 * Returns the meta object for the reference '{@link tdt4250.gr1700.location.config.LocationTaskConfig#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location</em>'.
	 * @see tdt4250.gr1700.location.config.LocationTaskConfig#getLocation()
	 * @see #getLocationTaskConfig()
	 * @generated
	 */
	EReference getLocationTaskConfig_Location();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.gr1700.location.config.LocationTaskConfig#getTargetDistance <em>Target Distance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Distance</em>'.
	 * @see tdt4250.gr1700.location.config.LocationTaskConfig#getTargetDistance()
	 * @see #getLocationTaskConfig()
	 * @generated
	 */
	EAttribute getLocationTaskConfig_TargetDistance();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.gr1700.location.config.LocationTaskConfig#getHints <em>Hints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hints</em>'.
	 * @see tdt4250.gr1700.location.config.LocationTaskConfig#getHints()
	 * @see #getLocationTaskConfig()
	 * @generated
	 */
	EReference getLocationTaskConfig_Hints();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ConfigFactory getConfigFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link tdt4250.gr1700.location.config.impl.LocationTaskConfigImpl <em>Location Task Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.gr1700.location.config.impl.LocationTaskConfigImpl
		 * @see tdt4250.gr1700.location.config.impl.ConfigPackageImpl#getLocationTaskConfig()
		 * @generated
		 */
		EClass LOCATION_TASK_CONFIG = eINSTANCE.getLocationTaskConfig();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCATION_TASK_CONFIG__LOCATION = eINSTANCE.getLocationTaskConfig_Location();

		/**
		 * The meta object literal for the '<em><b>Target Distance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCATION_TASK_CONFIG__TARGET_DISTANCE = eINSTANCE.getLocationTaskConfig_TargetDistance();

		/**
		 * The meta object literal for the '<em><b>Hints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCATION_TASK_CONFIG__HINTS = eINSTANCE.getLocationTaskConfig_Hints();

	}

} //ConfigPackage
