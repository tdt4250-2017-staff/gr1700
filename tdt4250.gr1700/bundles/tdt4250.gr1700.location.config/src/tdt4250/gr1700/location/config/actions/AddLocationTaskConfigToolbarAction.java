package tdt4250.gr1700.location.config.actions;

import no.hal.pg.config.actions.AddConfigObjectToolbarAction;
import tdt4250.gr1700.location.config.ConfigPackage;

public class AddLocationTaskConfigToolbarAction extends AddConfigObjectToolbarAction {
	
	public AddLocationTaskConfigToolbarAction() {
		super(ConfigPackage.eINSTANCE.getLocationTaskConfig(), no.hal.pg.config.ConfigPackage.eINSTANCE.getGameConfig_Tasks());
	}
}
