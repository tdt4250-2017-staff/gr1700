package tdt4250.gr1700.runtime.docker.test;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import tdt4250.gr1700.location.runtime.http.tests.AbstractResourceProviderTest;

public class ResourceProviderIT extends AbstractResourceProviderTest {

	@Before
	public void setUp() throws Exception {
		setUp(null, "runtime.port", true);
		System.out.println("Base URL: " + urlString);
	}

	@Test
	public void testRequests() throws IOException {
		super.testRequests();
	}
}
