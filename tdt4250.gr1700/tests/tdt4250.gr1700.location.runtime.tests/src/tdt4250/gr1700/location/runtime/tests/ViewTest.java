package tdt4250.gr1700.location.runtime.tests;

import java.io.IOException;
import java.util.Collection;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import no.hal.pg.app.GameView;
import no.hal.pg.osm.geoutil.LatLong;
import no.hal.pg.runtime.Player;
import tdt4250.gr1700.location.LocationTaskView;

public class ViewTest {

	private URI uri;
	private ResourceSet resourceSet;
	
	@Before
	public void setUp() {
		uri = URI.createURI(getClass().getResource("ViewTest.xmi").toString());
		resourceSet = new ResourceSetImpl();
	}

	protected Collection<EObject> loadRootObjects() {
		return loadRootObjects(uri);
	}

	protected Collection<EObject> loadRootObjects(URI uri) {
		Resource resource = resourceSet.createResource(uri);
		try {
			resource.load(null);
			return resource.getContents();
		} catch (IOException e) {
			Assert.fail();
		}
		return null;
	}
	
	@Test
	public void testLoad() {
		Collection<EObject> rootObjects = loadRootObjects();
		Assert.assertEquals(1, rootObjects.size());
		EObject first = rootObjects.iterator().next();
		Assert.assertTrue(first instanceof GameView<?>);
		GameView<?> gameView = (GameView<?>) first;
		Assert.assertEquals(1, gameView.getTaskViews().size());
		Assert.assertTrue(gameView.getTaskViews().get(0) instanceof LocationTaskView);
	}

	@Test
	public void testLocationViewUser() {
		Collection<EObject> rootObjects = loadRootObjects();
		GameView<?> gameView = (GameView<?>) rootObjects.iterator().next();
		LocationTaskView taskView = (LocationTaskView) gameView.getTaskViews().get(0);
		Assert.assertEquals(taskView.getUser(), gameView.getPlayer());
	}
	
	@Test
	public void testHint() {
		Collection<EObject> rootObjects = loadRootObjects();
		GameView<?> gameView = (GameView<?>) rootObjects.iterator().next();
		LocationTaskView taskView = (LocationTaskView) gameView.getTaskViews().get(0);
		Player player = taskView.getUser();
		LatLong latLong = taskView.getModel().getLatLong();
		Assert.assertEquals("More than 500 m away", taskView.getHint());

		// 122.78654233895249 meters away
		player.setLatitude((float) (latLong.latitude + 0.001));
		player.setLongitude((float) (latLong.longitude + 0.001));
		Assert.assertEquals("Within 200 m", taskView.getHint());

		// 1.3691104664079283 meters away
		player.setLatitude((float) (latLong.latitude + 0.00001));
		player.setLongitude((float) (latLong.longitude + 0.00001));
		Assert.assertEquals("Within 50 m", taskView.getHint());
	}
	
	@Test
	public void testTryLocation() {
		Collection<EObject> rootObjects = loadRootObjects();
		GameView<?> gameView = (GameView<?>) rootObjects.iterator().next();
		LocationTaskView taskView = (LocationTaskView) gameView.getTaskViews().get(0);
		taskView.getModel().start();
		Player player = taskView.getUser();
		LatLong latLong = taskView.getModel().getLatLong();

		// 122.78654233895249 meters away
		player.setLatitude((float) (latLong.latitude + 0.001));
		player.setLongitude((float) (latLong.longitude + 0.001));
		Assert.assertFalse(taskView.tryFinish());
		Assert.assertNull(taskView.getModel().getPlayer());
		Assert.assertFalse(taskView.getModel().isFinished());
		Assert.assertEquals(1, taskView.getModel().getAttemptCount());

		// 1.3691104664079283 meters away
		player.setLatitude((float) (latLong.latitude + 0.00001));
		player.setLongitude((float) (latLong.longitude + 0.00001));
		Assert.assertTrue(taskView.tryFinish());
		Assert.assertEquals(player, taskView.getModel().getPlayer());
		Assert.assertTrue(taskView.getModel().isFinished());
		Assert.assertEquals(2, taskView.getModel().getAttemptCount());
	}
	
	@Test
	public void testMultiTryLocation() {
		testTryLocation();
		URI otherUri = URI.createURI(getClass().getResource("ViewTest2.xmi").toString());
		Collection<EObject> rootObjects = loadRootObjects(otherUri);
		GameView<?> gameView = (GameView<?>) rootObjects.iterator().next();
		LocationTaskView taskView = (LocationTaskView) gameView.getTaskViews().get(0);
		taskView.getModel().start();
		Player player = taskView.getUser();
		LatLong latLong = taskView.getModel().getLatLong();

		// slightly farther
		player.setLatitude((float) (latLong.latitude + 0.000011));
		player.setLongitude((float) (latLong.longitude + 0.000011));
		Assert.assertFalse(taskView.tryFinish());
		Assert.assertNotEquals(player, taskView.getModel().getPlayer());
		Assert.assertTrue(taskView.getModel().isFinished());
		Assert.assertEquals(3, taskView.getModel().getAttemptCount());

		// closer
		player.setLatitude((float) (latLong.latitude + 0.000001));
		player.setLongitude((float) (latLong.longitude + 0.0000001));
		Assert.assertTrue(taskView.tryFinish());
		Assert.assertEquals(player, taskView.getModel().getPlayer());
		Assert.assertTrue(taskView.getModel().isFinished());
		Assert.assertEquals(4, taskView.getModel().getAttemptCount());
	}
}
