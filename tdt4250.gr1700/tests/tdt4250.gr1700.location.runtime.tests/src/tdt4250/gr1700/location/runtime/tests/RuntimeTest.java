package tdt4250.gr1700.location.runtime.tests;

import org.junit.Assert;
import org.junit.Test;

import no.hal.pg.runtime.Player;
import no.hal.pg.runtime.RuntimeFactory;
import tdt4250.gr1700.location.DistanceHint;
import tdt4250.gr1700.location.LocationFactory;
import tdt4250.gr1700.location.LocationTask;

public class RuntimeTest {

	@Test
	public void testCheckLocation() {
		LocationTask task = LocationFactory.eINSTANCE.createLocationTask();
		task.setLatitude(0);
		task.setLongitude(0);
		Player player1 = RuntimeFactory.eINSTANCE.createPlayer();
		player1.setLatitude(10);
		player1.setLongitude(10);
		task.start();

		try {
			task.checkPlayerLocation(player1);
			Assert.fail();
		} catch (Exception e) {
		}
		task.getPlayers().add(player1);
		Assert.assertFalse(task.isFinished());

		Assert.assertFalse(task.checkPlayerLocation(player1));
		Assert.assertEquals(1, task.getAttemptCount());
		Assert.assertNull(task.getPlayer());
		Assert.assertFalse(task.isFinished());

		player1.setLatitude(0.00001f);
		player1.setLongitude(0.00001f);
		Assert.assertTrue(task.checkPlayerLocation(player1));
		Assert.assertEquals(2, task.getAttemptCount());
		Assert.assertSame(player1, task.getPlayer());
		Assert.assertTrue(task.isFinished());
		double distance1 = task.getResult();
		
		Player player2 = RuntimeFactory.eINSTANCE.createPlayer();
		player1.setLatitude(0.000001f);
		player1.setLongitude(0.000001f);
		task.getPlayers().add(player2);
		Assert.assertTrue(task.checkPlayerLocation(player2));
		Assert.assertEquals(3, task.getAttemptCount());
		Assert.assertSame(player2, task.getPlayer());
		Assert.assertTrue(task.isFinished());
		double distance2 = task.getResult();
		Assert.assertTrue(distance2 < distance1);
	}
	
	@Test
	public void testGetHint() {
		LocationTask task = LocationFactory.eINSTANCE.createLocationTask();
		task.setLatitude(0);
		task.setLongitude(0);
		DistanceHint hint1 = LocationFactory.eINSTANCE.createDistanceHint();
		hint1.setDistance(10);
		hint1.setHint("10 m");
		task.getHints().add(hint1);
		DistanceHint hint2 = LocationFactory.eINSTANCE.createDistanceHint();
		hint2.setDistance(100);
		hint2.setHint("100 m");
		task.getHints().add(hint2);

		Assert.assertNull(task.getHint(101));

		DistanceHint hint3 = LocationFactory.eINSTANCE.createDistanceHint();
		hint3.setDistance(-1);
		hint3.setHint("Infinite");
		task.getHints().add(hint3);
		Assert.assertEquals("Infinite", task.getHint(1000));

		Assert.assertEquals("100 m", task.getHint(99));
		Assert.assertEquals("100 m", task.getHint(11));
		Assert.assertEquals("10 m", task.getHint(9));
	}
}
