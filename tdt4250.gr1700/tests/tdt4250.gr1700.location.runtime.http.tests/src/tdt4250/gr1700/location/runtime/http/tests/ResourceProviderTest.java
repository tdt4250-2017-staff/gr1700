package tdt4250.gr1700.location.runtime.http.tests;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class ResourceProviderTest extends AbstractResourceProviderTest {

	@Before
	public void setUp() throws Exception {
		setUp(null, "org.osgi.service.http.port", true);
	}

	@Test
	public void testRequests() throws IOException {
		super.testRequests();
	}
}
