package tdt4250.gr1700.location.runtime.http.tests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;

import org.junit.Assert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class AbstractResourceProviderTest extends AbstractHttpRequestTest {

	@Override
	protected String getGameName() {
		return "ViewTest";
	}

	protected void testRequests() throws IOException {
		int attemptsLeft = 5;
		while (attemptsLeft > 0) {
			HttpURLConnection con1 = getRequest("");
			try {
				attemptsLeft--;
				testGameView(con1);
				break;
			} catch (Exception e) {
				if (attemptsLeft <= 0) {
					Assert.fail(e.getMessage() + " for " + urlString);
				}
				try {
					Thread.sleep(500);
				} catch (InterruptedException ignore) {
				}
			}
		}
		// location is 63.41675, 10.404964
		testLocationTaskViewEnabled(getRequest("/taskViews/0"));
		testLocationTaskViewStarted(getRequest("/taskViews/0/start"));
		// latitude + 0.001, longitude + 0.001
		testLocationTaskViewSetPlayerLocation1(getRequest("/taskViews/0/setPlayerLocation?latitude=63.41775&longitude=10.405964"));
		testLocationTaskViewTryFinish1(getRequest("/taskViews/0/tryFinish"));
		// latitude + 0.00001, longitude + 0.00001
		testLocationTaskViewSetPlayerLocation2(getRequest("/taskViews/0/setPlayerLocation?latitude=63.416751&longitude=10.404965"));
		testLocationTaskViewTryFinish2(getRequest("/taskViews/0/tryFinish"));
	}

	private boolean useOwnReader = true;
	private ObjectMapper mapper = new ObjectMapper();

	protected void outputJsonNode(JsonNode jsonNode, PrintStream output) {
		try {
			output.println(mapper.writeValueAsString(jsonNode));
		} catch (JsonProcessingException e) {
		}
	}

	protected void outputJsonNode(JsonNode jsonNode) {
		outputJsonNode(jsonNode, System.out);
	}

	protected JsonNode getJsonNode(HttpURLConnection con) throws IOException {
		InputStream input = con.getInputStream();
		JsonNode jsonNode = (useOwnReader ? mapper.readTree(new BufferedReader(new InputStreamReader(input))) : mapper.readTree(input));
		return jsonNode;
	}
	
	protected void testGameView(HttpURLConnection con) throws IOException {
		JsonNode jsonNode = getJsonNode(con);
		ArrayNode rootNode = checkArrayNode(jsonNode, 1);
		checkObjectNode(rootNode.get(0)); // empty lists don't serialize: "players", "items"
	}

	protected void testLocationTaskViewEnabled(HttpURLConnection con) throws IOException {
		JsonNode jsonNode = getJsonNode(con);
		ArrayNode rootNode = checkArrayNode(jsonNode, 1);
		checkObjectNode(rootNode.get(0), "locationLatitude", "locationLongitude", "description", "hint", "mapView");
		checkObjectNode(rootNode.get(0), "enabled", true, "started", false, "finished", false);
	}
	
	protected void testLocationTaskViewStarted(HttpURLConnection con) throws IOException {
		JsonNode jsonNode = getJsonNode(con);
		ArrayNode rootNode = checkArrayNode(jsonNode, 1);
		checkObjectNode(rootNode.get(0), "enabled", true, "started", true, "finished", false);
	}

	protected void testLocationTaskViewSetPlayerLocation1(HttpURLConnection con) throws IOException {
		JsonNode jsonNode = getJsonNode(con);
		ArrayNode rootNode = checkArrayNode(jsonNode, 1);
		checkObjectNode(rootNode.get(0), "hint", (Object) "\"Within 200 m\"");
	}
	
	protected void testLocationTaskViewTryFinish1(HttpURLConnection con) throws IOException {
		JsonNode jsonNode = getJsonNode(con);
		ArrayNode rootNode = checkArrayNode(jsonNode, 1);
		checkBooleanNode(rootNode.get(0), false);
	}
	
	protected void testLocationTaskViewSetPlayerLocation2(HttpURLConnection con) throws IOException {
		JsonNode jsonNode = getJsonNode(con);
		ArrayNode rootNode = checkArrayNode(jsonNode, 1);
		checkObjectNode(rootNode.get(0), "hint", (Object) "\"Within 50 m\"");
	}
	
	protected void testLocationTaskViewTryFinish2(HttpURLConnection con) throws IOException {
		JsonNode jsonNode = getJsonNode(con);
		ArrayNode rootNode = checkArrayNode(jsonNode, 1);
		checkBooleanNode(rootNode.get(0), true);
	}
}
