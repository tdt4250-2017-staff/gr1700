# Example project(s) for TDT4250

## Overview
This is the example provided for TDT4250 in 2017, with projects corresponding to what is expected from the student projects. Think of it as group 00's project, manned by the course staff. Students can use it to learn from and/or as a template for their own projects.

## Structure

The project layout is based on the one used by Eclipse projects:
- [bundles](bundles/) - the "productive" application code
- [tests](tests/) - test projects
- [features](features/) - feature projects, used for configuration and installation purposes
- [releng](releng/) - projects related to release engineering
